import copy
import json
import re
from unittest import TestCase

from main import *


class DevelopmentTest(TestCase):
    # def testUniformLayoutFactory(self):
    #     layout = UniformLayoutFactory().create(HostFactory())
    #     layout.getHosts().append(Host(0, 0))
    #     layout.plot()
    #
    # def testTopologyPlot(self):
    #     layout = UniformLayoutFactory().create(HostFactory())
    #     topology = FullMeshTopologyFactory().create(layout, LinkFactory())
    #     topology.plot()
    #
    # def testTopologyGetNeighbours(self):
    #     layout = UniformLayoutFactory().create(HostFactory())
    #     topology = FullMeshTopologyFactory().create(layout, LinkFactory())
    #     neighbors = layout.getHosts()[0].getNeighbors()
    #     self.assertEqual(19, len(neighbors))
    #
    # def testTopologyGetDensity(self):
    #     layout = UniformLayoutFactory().create(HostFactory())
    #     topology = FullMeshTopologyFactory().create(layout, LinkFactory())
    #     self.assertEqual(19, topology.getAverageVertexDegree())
    #
    # def testGabrielGraph(self):
    #     layout = UniformLayoutFactory().create(HostFactory())
    #     topology = GabrielGraphTopologyFactory().create(layout, LinkFactory())
    #     print(topology.getAverageVertexDegree())
    #     topology.plot()
    #
    # def testAODVRouting(self):
    #
    #     # layout = Layout(100, 100)
    #     # hostFactory = HostFactory()
    #     # layout.addHost(hostFactory.create(0, 1))  # 0
    #     # layout.addHost(hostFactory.create(1, 0))  # 1
    #     # layout.addHost(hostFactory.create(1, 1))  # 2
    #     # layout.addHost(hostFactory.create(1, 2))  # 3
    #     # layout.addHost(hostFactory.create(2, 0))  # 4
    #     # layout.addHost(hostFactory.create(2, 1))  # 5
    #     # layout.addHost(hostFactory.create(2, 2))  # 6
    #     # topology = Topology(layout)
    #     # topology.addLink(Link(layout.getHosts()[0], layout.getHosts()[1]))
    #     # topology.addLink(Link(layout.getHosts()[0], layout.getHosts()[2]))
    #     # topology.addLink(Link(layout.getHosts()[0], layout.getHosts()[3]))
    #     # topology.addLink(Link(layout.getHosts()[1], layout.getHosts()[4]))
    #     # topology.addLink(Link(layout.getHosts()[1], layout.getHosts()[5]))
    #     # topology.addLink(Link(layout.getHosts()[2], layout.getHosts()[4]))
    #     # topology.addLink(Link(layout.getHosts()[2], layout.getHosts()[5]))
    #     # topology.addLink(Link(layout.getHosts()[2], layout.getHosts()[6]))
    #     # topology.addLink(Link(layout.getHosts()[3], layout.getHosts()[5]))
    #     # topology.addLink(Link(layout.getHosts()[3], layout.getHosts()[6]))
    #
    #     layout = UniformLayoutFactory(0, 20).create(HostFactory(HostRoutingType.AODV))
    #     topology = GabrielGraphTopologyFactory().create(layout, LinkFactory())
    #
    #     layout.plot()
    #     topology.plot()
    #
    #
    #     params = RoutingConfiguration(sinkId=0,
    #                                   communicationModel=CommunicationModel.SINK,
    #                                   destinationOnlyFlag=False
    #                                   )
    #     routing = AODVRouting(topology, params)
    #     # paths = routing.dijkstra(layout.getHosts()[0])
    #     # for host in layout.getHosts():
    #     #     routing.appendPath(paths[host])
    #
    #     metric = AveragePathPDRMetric(routing)
    #     results = Results()
    #     scenario = FiveBadHostsScenario(metric)
    #     scenario.setId(0)
    #     scenario.setResults(results)
    #     scenario.execute()
    #
    #     # routing.generatePaths()
    #     # paths = routing.getPaths()
    #     # for host in layout.getHosts():
    #     #     print("{0}:{1}".format(host, len(host.getPaths())))
    #     # plt.show()
    #     # pass
    #
    # def testAODVPure(self):
    #     configuration = Configuration()
    #     configuration.routingConfiguration.destinationOnlyFlag = True
    #     sim = Simulation("Pure AODV")
    #     sim.run(configuration, 0, 10)
    #
    #     configuration = Configuration()
    #     configuration.routingConfiguration.destinationOnlyFlag = True
    #     configuration.hostProtoDelayType = HostProtoDelayType.PDR1
    #     sim = Simulation("Penalized AODV")
    #     sim.run(configuration, 0, 10)
    #
    # def testCustom1(self):
    #     configuration = Configuration()
    #     configuration.hostProtoDelayType = HostProtoDelayType.PDR1
    #     configuration.hostProtoDelayConfiguration.constDelay = 100
    #     configuration.layoutType = LayoutType.CUSTOM1
    #     configuration.topologyType = TopologyType.CUSTOM1
    #     configuration.scenarioType = ScenarioType.TEST
    #     configuration.routingConfiguration.destinationOnlyFlag = False
    #     sim = Simulation("Single instance - with protocol")
    #     sim.run(configuration, 0, 1)
    #
    # def testCustom2(self):
    #     configuration = Configuration()
    #     configuration.hostProtoDelayType = HostProtoDelayType.PDR1
    #     configuration.hostProtoDelayConfiguration.constDelay = 100
    #     configuration.layoutType = LayoutType.CUSTOM2
    #     configuration.topologyType = TopologyType.CUSTOM2
    #     configuration.routingConfiguration.destinationOnlyFlag = False
    #     configuration.scenarioType = ScenarioType.CUSTOM2
    #     sim = Simulation("Custom2 scenario")
    #     sim.run(configuration, 0, 1)
    #     pass
    #
    # def testBetaSkeletonCircleBased(self):
    #     configuration = Configuration()
    #     configuration.seedId = 0
    #     configuration.hostQuantity = 100
    #     configuration.topologyConfiguration.beta = 1
    #     configuration.hostProtoDelayType = HostProtoDelayType.PDR1
    #     configuration.hostProtoDelayConfiguration.constDelay = 100
    #     configuration.topologyType = TopologyType.BETA_SKELETON
    #     configuration.scenarioType = ScenarioType.FIVE_BAD_HOSTS
    #     configuration.routingConfiguration.destinationOnlyFlag = False
    #     factory = MultiFactory()
    #     scenario = factory.createScenario(configuration)
    #     scenario.execute()
    #     scenario.plot()
    #     plt.show()
    #
    #     # print(scenario.getMetric().calculate())
    #     #
    #     # print(len(scenario.getMetric().getRouting().getTopology().getLinks()))
    #     # print(scenario.getMetric().getRouting().getTopology().getAverageVertexDegree())
    #     # print(scenario.getMetric().getRouting().getTopology().getDensity())
    #     scenario.execute()
    #
    # def testCompareFiveBad(self):
    #     configuration = Configuration()
    #     configuration.plot = True
    #     configuration.hostQuantity = 20
    #     configuration.layoutType = LayoutType.UNIFORM
    #     configuration.topologyType = TopologyType.BETA_SKELETON
    #     configuration.topologyConfiguration.beta = 1
    #     configuration.routingConfiguration.destinationOnlyFlag = False
    #     configuration.scenarioType = ScenarioType.FIVE_BAD_HOSTS
    #
    #     configuration.simulationName = "Five bad - no trust"
    #     sim = Simulation()
    #     sim.run(configuration, 0, 10, silent=False)
    #
    #     configuration.hostProtoDelayType = HostProtoDelayType.PDR1
    #     configuration.hostProtoDelayConfiguration.constDelay = 100
    #
    #     configuration.simulationName = "Five bad - with trust"
    #     sim = Simulation()
    #     sim.run(configuration, 0, 10, silent=False)
    #
    # def testAdjustableScenario(self):
    #     configuration = Configuration()
    #     configuration.simulationName = "Adjustable scenario test - no trust"
    #     configuration.resultsDirectory = "Results"
    #     configuration.plot = True
    #     configuration.seedId = 0  # Not necessary to change, it is changed by simulation anyway
    #
    #     configuration.xSize = 100  # the size of the area, where are the hosts generated
    #     configuration.ySize = 100
    #     configuration.hostQuantity = 100
    #     configuration.hostRoutingType = HostRoutingType.AODV
    #     configuration.hostProtoDelayType = HostProtoDelayType.DEFAULT  # the default proto delay is 0
    #     # configuration.hostProtoDelayConfiguration.constDelay = 100  # required by PDR1HostProtoDelay
    #     # configuration.hostProtoDelayConfiguration.maxDelay = 10  # required by PDR2HostProtoDelay
    #     configuration.hostCommDelayType = HostCommDelayType.DEFAULT  # the default communication delay is 1
    #     # configuration.hostCommDelayConfiguration = HostCommDelayConfiguration()  # not used so far
    #     configuration.layoutType = LayoutType.UNIFORM  # generates the hosts uniformly over the area
    #
    #     configuration.linkType = LinkType.DEFAULT
    #     configuration.topologyType = TopologyType.BETA_SKELETON
    #     configuration.topologyConfiguration.beta = 1
    #
    #     configuration.routingType = RoutingType.AODV
    #     configuration.communicationModel = CommunicationModel.SINK
    #     configuration.destinationOnlyFlag = False
    #     configuration.routingConfiguration.sinkId = 0
    #     configuration.routingConfiguration.communicationModel = CommunicationModel.SINK
    #     configuration.routingConfiguration.destinationOnlyFlag = False
    #
    #     configuration.metricType = MetricType.AVERAGE_PATH_PDR
    #
    #     configuration.scenarioType = ScenarioType.ADJUSTABLE
    #     configuration.scenarioConfiguration.ratio = 0.05
    #     configuration.scenarioConfiguration.pdrMin = 0.5
    #     configuration.scenarioConfiguration.pdrMax = 0.5
    #     configuration.results = Results()
    #
    #     sim = Simulation()
    #     sim.run(configuration, 0, 10, silent=True)
    #
    #     configuration.simulationName = "Adjustable scenario test - with trust"
    #     configuration.hostProtoDelayType = HostProtoDelayType.PDR2
    #     configuration.hostProtoDelayConfiguration.maxDelay = 20
    #     configuration.hostProtoDelayConfiguration.constDelay = 100
    #
    #     sim = Simulation()
    #     sim.run(configuration, 0, 10, silent=True)
    #
    # def testCommDelay(self):
    #     configuration = Configuration()
    #     configuration.simulationName = "Communication delay - no PDR"
    #     configuration.resultsDirectory = "Results"
    #     configuration.plot = True
    #     configuration.seedId = 0  # Not necessary to change, it is changed by simulation anyway
    #
    #     configuration.xSize = 100  # the size of the area, where are the hosts generated
    #     configuration.ySize = 100
    #     configuration.hostQuantity = 100
    #     # configuration.hostRoutingType = HostRoutingType.AODV
    #
    #     configuration.hostProtoDelayType = HostProtoDelayType.DEFAULT  # the default proto delay is 0
    #     # configuration.hostProtoDelayConfiguration.constDelay = 100  # required by PDR1HostProtoDelay
    #     # configuration.hostProtoDelayConfiguration.maxDelay = 10  # required by PDR2HostProtoDelay
    #     configuration.hostCommDelayType = HostCommDelayType.DEFAULT  # the default communication delay is 1
    #     # configuration.hostCommDelayConfiguration = HostCommDelayConfiguration()  # not used so far
    #     configuration.layoutType = LayoutType.UNIFORM  # generates the hosts uniformly over the area
    #
    #     # configuration.linkType = LinkType.DEFAULT
    #     # configuration.topologyType = TopologyType.BETA_SKELETON
    #     configuration.topologyConfiguration.beta = 1  # network density: 0.7-2
    #
    #     # configuration.routingType = RoutingType.AODV
    #     # configuration.routingConfiguration.sinkId = 0
    #     # configuration.routingConfiguration.communicationModel = CommunicationModel.SINK
    #     configuration.routingConfiguration.destinationOnlyFlag = False
    #
    #     # configuration.metricType = MetricType.AVERAGE_PATH_PDR
    #
    #     configuration.scenarioType = ScenarioType.ADJUSTABLE
    #     configuration.scenarioConfiguration.ratio = 0.05
    #     configuration.scenarioConfiguration.pdrMin = 0.5
    #     configuration.scenarioConfiguration.pdrMax = 0.5
    #     # configuration.results = Results()
    #
    #     sim = Simulation()
    #     sim.run(configuration, 0, 10, silent=False)
    #
    #     configuration.simulationName = "Communication delay - with PDR"
    #     configuration.hostCommDelayType = HostCommDelayType.PDR
    #
    #     sim = Simulation()
    #     sim.run(configuration, 0, 10, silent=False)
    #
    #
    # def testHostProtoPDR3Delay(self):
    #     configuration = Configuration()
    #     configuration.simulationName = "No delay based on threshold"
    #     configuration.resultsDirectory = "Results"
    #     configuration.plot = True
    #     configuration.seedId = 0  # Not necessary to change, it is changed by simulation anyway
    #
    #     configuration.xSize = 100  # the size of the area, where are the hosts generated
    #     configuration.ySize = 100
    #     configuration.hostQuantity = 10
    #     # configuration.hostRoutingType = HostRoutingType.AODV
    #     configuration.hostProtoDelayType = HostProtoDelayType.DEFAULT  # the default proto delay is 0
    #     # configuration.hostProtoDelayConfiguration.constDelay = 100  # required by PDR1HostProtoDelay
    #     # configuration.hostProtoDelayConfiguration.maxDelay = 10  # required by PDR2HostProtoDelay
    #     configuration.hostCommDelayType = HostCommDelayType.DEFAULT  # the default communication delay is 1
    #     # configuration.hostCommDelayConfiguration = HostCommDelayConfiguration()  # not used so far
    #     configuration.layoutType = LayoutType.UNIFORM  # generates the hosts uniformly over the area
    #
    #     # configuration.linkType = LinkType.DEFAULT
    #     # configuration.topologyType = TopologyType.BETA_SKELETON
    #     configuration.topologyConfiguration.beta = 1  # network density: 0.7-2
    #
    #     # configuration.routingType = RoutingType.AODV
    #     # configuration.routingConfiguration.sinkId = 0
    #     # configuration.routingConfiguration.communicationModel = CommunicationModel.SINK
    #     configuration.routingConfiguration.destinationOnlyFlag = False
    #
    #     # configuration.metricType = MetricType.AVERAGE_PATH_PDR
    #
    #     configuration.scenarioType = ScenarioType.ADJUSTABLE
    #     configuration.scenarioConfiguration.ratio = 0.2
    #     configuration.scenarioConfiguration.pdrMin = 0.5
    #     configuration.scenarioConfiguration.pdrMax = 0.5
    #     # configuration.results = Results()
    #
    #     configuration.hostCommDelayType = HostCommDelayType.PDR
    #
    #     sim = Simulation()
    #     sim.run(configuration, 0, 1000, silent=False)
    #
    #     configuration.simulationName = "Delay based on threshold"
    #     configuration.hostProtoDelayType = HostProtoDelayType.PDR3
    #     configuration.hostProtoDelayConfiguration.threshold = 0.8
    #     configuration.hostProtoDelayConfiguration.constDelay = 10
    #
    #     sim = Simulation()
    #     sim.run(configuration, 0, 10, silent=False)

    def runConfigurations(self, configurations, silent=True):
        for configuration in configurations:
            sim = Simulation(configuration)
            sim.run(silent=silent)

    def testMultipleConfigurations(self):
        configurations = []

        for beta in [1, 1.1, 1.2]:
            configurations.append(Configuration.create(simulationName="Beta variation {0}".format(beta),
                                                       resulsDirectory="Results",
                                                       plot=True,
                                                       startId=0,
                                                       quantity=10,
                                                       hostQuantity=100,
                                                       hostProtoDelayType=HostProtoDelayType.DEFAULT,
                                                       # hostProtoDelayConstDelay=10,
                                                       # hostProtoDelayMaxDelay=10,
                                                       # hostProtoDelayThreshold=0.8,
                                                       hostCommDelayType=HostCommDelayType.DEFAULT,
                                                       beta=beta,
                                                       destinationOnlyFlag=False,
                                                       scenarioRatio=0.2,  # 0 means all hosts have PDR=1
                                                       scenarioPdrMin=0.5,
                                                       scenarioPdrMax=0.5,
                                                       ))
        # configurations.append(Configuration.create(simulationName="No delay based on threshold",
        #                                            resulsDirectory="Results",
        #                                            plot=True,
        #                                            startId=0,
        #                                            quantity=10,
        #                                            hostQuantity=100,
        #                                            hostProtoDelayType=HostProtoDelayType.DEFAULT,
        #                                            # hostProtoDelayConstDelay=10,
        #                                            # hostProtoDelayMaxDelay=10,
        #                                            # hostProtoDelayThreshold=0.8,
        #                                            hostCommDelayType=HostCommDelayType.DEFAULT,
        #                                            beta=1,
        #                                            destinationOnlyFlag=False,
        #                                            scenarioRatio=0.2,  # 0 means all hosts have PDR=1
        #                                            scenarioPdrMin=0.5,
        #                                            scenarioPdrMax=0.5,
        #                                            ))
        #
        # configurations.append(Configuration.create(simulationName="With delay based on threshold",
        #                                            resulsDirectory="Results",
        #                                            plot=True,
        #                                            startId=0,
        #                                            quantity=10,
        #                                            hostQuantity=100,
        #                                            hostProtoDelayType=HostProtoDelayType.PDR3,
        #                                            hostProtoDelayConstDelay=10,
        #                                            # hostProtoDelayMaxDelay=10,
        #                                            hostProtoDelayThreshold=0.8,
        #                                            hostCommDelayType=HostCommDelayType.DEFAULT,
        #                                            beta=1,
        #                                            destinationOnlyFlag=False,
        #                                            scenarioRatio=0.2,  # 0 means all hosts have PDR=1
        #                                            scenarioPdrMin=0.5,
        #                                            scenarioPdrMax=0.5,
        #                                            ))

        self.runConfigurations(configurations, silent=True)

    def testDensityDistribution(self):
        print("To run me, comment out 'return' on the next line...")
        return

        configurations = []
        for bigBeta in range(10, 21):
            beta = bigBeta / 10
            configurations.append(Configuration.create(simulationName="Density test - beta: {0}".format(beta),
                                                       resulsDirectory="Results",
                                                       plot=True,
                                                       startId=0,
                                                       quantity=100,
                                                       hostQuantity=100,
                                                       hostProtoDelayType=HostProtoDelayType.DEFAULT,
                                                       # hostProtoDelayConstDelay=10,
                                                       # hostProtoDelayMaxDelay=10,
                                                       # hostProtoDelayThreshold=0.8,
                                                       hostCommDelayType=HostCommDelayType.DEFAULT,
                                                       beta=beta,
                                                       destinationOnlyFlag=False,
                                                       scenarioType=ScenarioType.SHOW_TOPOLOGY_PROPERTIES,
                                                       scenarioRatio=0.2,  # 0 means all hosts have PDR=1
                                                       scenarioPdrMin=0.5,
                                                       scenarioPdrMax=0.5,
                                                       ))

        self.runConfigurations(configurations)

    def test1(self):
        print("To run me, comment out 'return' on the next line...")
        return

        configurationsEmpty = []
        for bigBeta in [10, 20]:
            beta = bigBeta / 10
            configurationsEmpty.append(Configuration.create(simulationName="MX-SX-DFX-{0}".format(beta),
                                                            resulsDirectory="Testing1",
                                                            plot=True,
                                                            startId=0,
                                                            quantity=10,
                                                            hostQuantity=100,
                                                            hostProtoDelayType=HostProtoDelayType.DEFAULT,
                                                            hostProtoDelayConstDelay=10,
                                                            hostProtoDelayMaxDelay=10,
                                                            hostProtoDelayThreshold=0.8,
                                                            hostProtoDelayMinDelay=0,
                                                            hostProtoDelayBase=100,
                                                            hostCommDelayType=HostCommDelayType.DEFAULT,
                                                            beta=beta,
                                                            destinationOnlyFlag=False,
                                                            scenarioType=ScenarioType.SHOW_TOPOLOGY_PROPERTIES,
                                                            scenarioRatio=0.2,  # 0 means all hosts have PDR=1
                                                            scenarioPdrMin=0.5,
                                                            scenarioPdrMax=0.5,
                                                            ))
        configurationsM = []
        # No trust (M1)
        for c in configurationsEmpty:
            configuration = copy.deepcopy(c)
            configuration.simulationName = re.sub(r"MX", "M1", c.simulationName)
            configurationsM.append(configuration)

        # Const delay (M2)
        for c in configurationsEmpty:
            configuration = copy.deepcopy(c)
            configuration.simulationName = re.sub(r"MX", "M2", c.simulationName)
            configuration.hostProtoDelayType = HostProtoDelayType.CONST
            configuration.hostProtoDelayConfiguration.threshold = 1.0
            configurationsM.append(configuration)

        # Const delay with threshold (M3)
        for c in configurationsEmpty:
            configuration = copy.deepcopy(c)
            configuration.simulationName = re.sub(r"MX", "M3", c.simulationName)
            configuration.hostProtoDelayType = HostProtoDelayType.CONST
            configuration.hostProtoDelayConfiguration.threshold = 0.8
            configurationsM.append(configuration)

        # Linear delay (M4)
        for c in configurationsEmpty:
            configuration = copy.deepcopy(c)
            configuration.simulationName = re.sub(r"MX", "M4", c.simulationName)
            configuration.hostProtoDelayType = HostProtoDelayType.LINEAR
            configuration.hostProtoDelayConfiguration.threshold = 1.0
            configurationsM.append(configuration)

        # Linear delay with threshold (M5)
        for c in configurationsEmpty:
            configuration = copy.deepcopy(c)
            configuration.simulationName = re.sub(r"MX", "M5", c.simulationName)
            configuration.hostProtoDelayType = HostProtoDelayType.LINEAR
            configuration.hostProtoDelayConfiguration.threshold = 0.8
            configurationsM.append(configuration)

        # Exponential delay (M6)
        for c in configurationsEmpty:
            configuration = copy.deepcopy(c)
            configuration.simulationName = re.sub(r"MX", "M6", c.simulationName)
            configuration.hostProtoDelayType = HostProtoDelayType.EXPONENTIAL
            configuration.hostProtoDelayConfiguration.threshold = 1.0
            configurationsM.append(configuration)

        # Exponential delay with threshold(M7)
        for c in configurationsEmpty:
            configuration = copy.deepcopy(c)
            configuration.simulationName = re.sub(r"MX", "M7", c.simulationName)
            configuration.hostProtoDelayType = HostProtoDelayType.EXPONENTIAL
            configuration.hostProtoDelayConfiguration.threshold = 0.8
            configurationsM.append(configuration)

        configurationsS = []

        # One half 50%
        for c in configurationsM:
            configuration = copy.deepcopy(c)
            configuration.simulationName = re.sub(r"SX", "S1", c.simulationName)
            configuration.scenarioType = ScenarioType.ADJUSTABLE
            configuration.scenarioConfiguration.ratio = 0.5
            configuration.scenarioConfiguration.pdrMin = 0.5
            configuration.scenarioConfiguration.pdrMax = 0.5
            configurationsS.append(configuration)

        # One half 0-100%
        for c in configurationsM:
            configuration = copy.deepcopy(c)
            configuration.simulationName = re.sub(r"SX", "S2", c.simulationName)
            configuration.scenarioType = ScenarioType.ADJUSTABLE
            configuration.scenarioConfiguration.ratio = 0.5
            configuration.scenarioConfiguration.pdrMin = 0.0
            configuration.scenarioConfiguration.pdrMax = 1.0
            configurationsS.append(configuration)

        # All 0-100%
        for c in configurationsM:
            configuration = copy.deepcopy(c)
            configuration.simulationName = re.sub(r"SX", "S3", c.simulationName)
            configuration.scenarioType = ScenarioType.ADJUSTABLE
            configuration.scenarioConfiguration.ratio = 1
            configuration.scenarioConfiguration.pdrMin = 0.0
            configuration.scenarioConfiguration.pdrMax = 1.0
            configurationsS.append(configuration)

        configurations = []
        # Destination Flag True
        for c in configurationsS:
            configuration = copy.deepcopy(c)
            configuration.simulationName = re.sub(r"DFX", "DFT", c.simulationName)
            configuration.routingConfiguration.destinationOnlyFlag = True
            configurations.append(configuration)

        # Destination Flag False
        for c in configurationsS:
            configuration = copy.deepcopy(c)
            configuration.simulationName = re.sub(r"DFX", "DFF", c.simulationName)
            configuration.routingConfiguration.destinationOnlyFlag = False
            configurations.append(configuration)

        self.runConfigurations(configurations)

    def testAnomaly(self):
        # print("To run me, comment out 'return' on the next line...")
        # return

        resultsDir = "R-Linear-Anomaly"
        hostNumber = 30
        quantity = 1
        startId = 6

        configurations = []
        maxDelay = 10
        threshold = 0.8
        configurations.append(Configuration.create(simulationName="Control",
                                                   resulsDirectory=resultsDir,
                                                   plot=True,
                                                   startId=startId,
                                                   quantity=quantity,
                                                   hostQuantity=hostNumber,
                                                   hostProtoDelayType=HostProtoDelayType.DEFAULT,
                                                   hostProtoDelayConstDelay=10,
                                                   hostProtoDelayMaxDelay=maxDelay,
                                                   hostProtoDelayThreshold=threshold,
                                                   hostProtoDelayMinDelay=0,
                                                   hostProtoDelayBase=100,
                                                   hostCommDelayType=HostCommDelayType.DEFAULT,
                                                   beta=1,
                                                   destinationOnlyFlag=True,
                                                   scenarioType=ScenarioType.ADJUSTABLE,
                                                   scenarioRatio=0.5,  # 0 means all hosts have PDR=1
                                                   scenarioPdrMin=0,
                                                   scenarioPdrMax=1,


                                                   ))
        self.runConfigurations(configurations)
        # return

        configurations = []
        for threshold in [0.7, 0.75, 0.8]:  # 1.0, 0.95, 0.9, 0.85, 0.8, 0.75, 0.7, 0.65, 0.6, 0.55, 0.5]:
            for maxDelay in [10]:
                configurations.append(
                    Configuration.create(simulationName="THR:{:04.2f}-MD:{:02d}".format(threshold, maxDelay),
                                         resulsDirectory=resultsDir,
                                         plot=True,
                                         startId=startId,
                                         quantity=quantity,
                                         hostQuantity=hostNumber,
                                         hostProtoDelayType=HostProtoDelayType.LINEAR,
                                         hostProtoDelayConstDelay=10,
                                         hostProtoDelayMaxDelay=maxDelay,
                                         hostProtoDelayThreshold=threshold,
                                         hostProtoDelayMinDelay=0,
                                         hostProtoDelayBase=100,
                                         hostCommDelayType=HostCommDelayType.DEFAULT,
                                         beta=1,
                                         destinationOnlyFlag=True,
                                         scenarioType=ScenarioType.ADJUSTABLE,
                                         scenarioRatio=0.5,  # 0 means all hosts have PDR=1
                                         scenarioPdrMin=0,
                                         scenarioPdrMax=1,
                                         ))
        self.runConfigurations(configurations)

    def testConstantDelay(self, df):
        # print("To run me, comment out 'return' on the next line...")
        # return
        if df:
            DF = "DFT"
        else:
            DF = "DFF"
        resultsDir = "R-Constant-delay-{}".format(DF)
        hostNumber = 100
        quantity = 100
        startId = 100

        configurations = []
        constDelay = None
        threshold = None
        configurations.append(Configuration.create(simulationName="Control",
                                                   resulsDirectory=resultsDir,
                                                   plot=False,
                                                   startId=startId,
                                                   quantity=quantity,
                                                   hostQuantity=hostNumber,
                                                   hostProtoDelayType=HostProtoDelayType.DEFAULT,
                                                   hostProtoDelayConstDelay=10,
                                                   hostProtoDelayMaxDelay=constDelay,
                                                   hostProtoDelayThreshold=threshold,
                                                   hostProtoDelayMinDelay=0,
                                                   hostProtoDelayBase=100,
                                                   hostCommDelayType=HostCommDelayType.DEFAULT,
                                                   beta=1,
                                                   destinationOnlyFlag=df,
                                                   scenarioType=ScenarioType.ADJUSTABLE,
                                                   scenarioRatio=0.5,  # 0 means all hosts have PDR=1
                                                   scenarioPdrMin=0,
                                                   scenarioPdrMax=1,


                                                   ))
        self.runConfigurations(configurations)
        # return

        configurations = []
        # for threshold in [1.0, 0.9,  0.8, 0.7, 0.6, 0.5]:  # [1.0, 0.95, 0.9, 0.85, 0.8, 0.75, 0.7, 0.65, 0.6, 0.55, 0.5]:
        #     for constDelay in [1, 2, 4, 6, 8, 10, 20, 40, 80, 160, 320]:
        for threshold in [1.0, 0.9, 0.8, 0.7, 0.6, 0.5]:
            for constDelay in [1, 2, 4, 8, 16, 32, 64, 128, 254, 512]:
                configurations.append(
                    Configuration.create(simulationName="THR:{:04.2f}-CD:{:02d}".format(threshold, constDelay),
                                         resulsDirectory=resultsDir,
                                         plot=False,
                                         startId=startId,
                                         quantity=quantity,
                                         hostQuantity=hostNumber,
                                         hostProtoDelayType=HostProtoDelayType.CONST,
                                         hostProtoDelayConstDelay=constDelay,
                                         hostProtoDelayMaxDelay=None,
                                         hostProtoDelayThreshold=threshold,
                                         hostProtoDelayMinDelay=0,
                                         hostProtoDelayBase=100,
                                         hostCommDelayType=HostCommDelayType.DEFAULT,
                                         beta=1,
                                         destinationOnlyFlag=df,
                                         scenarioType=ScenarioType.ADJUSTABLE,
                                         scenarioRatio=0.5,  # 0 means all hosts have PDR=1
                                         scenarioPdrMin=0,
                                         scenarioPdrMax=1,
                                         ))
        self.runConfigurations(configurations)

    def testLinearDelay(self, df):
        # print("To run me, comment out 'return' on the next line...")
        # return

        if df:
            DF = "DFT"
        else:
            DF = "DFF"
        resultsDir = "R-Linear-delay-{}".format(DF)
        hostNumber = 100
        quantity = 100
        startId = 100

        configurations = []
        maxDelay = None
        threshold = None
        configurations.append(Configuration.create(simulationName="Control",
                                                   resulsDirectory=resultsDir,
                                                   plot=False,
                                                   startId=startId,
                                                   quantity=quantity,
                                                   hostQuantity=hostNumber,
                                                   hostProtoDelayType=HostProtoDelayType.DEFAULT,
                                                   hostProtoDelayConstDelay=10,
                                                   hostProtoDelayMaxDelay=maxDelay,
                                                   hostProtoDelayThreshold=threshold,
                                                   hostProtoDelayMinDelay=0,
                                                   hostProtoDelayBase=100,
                                                   hostCommDelayType=HostCommDelayType.DEFAULT,
                                                   beta=1,
                                                   destinationOnlyFlag=df,
                                                   scenarioType=ScenarioType.ADJUSTABLE,
                                                   scenarioRatio=0.5,  # 0 means all hosts have PDR=1
                                                   scenarioPdrMin=0,
                                                   scenarioPdrMax=1,


                                                   ))
        self.runConfigurations(configurations)
        # return

        configurations = []
        # for threshold in [1.0, 0.9,  0.8, 0.7, 0.6, 0.5]:  # 1.0, 0.95, 0.9, 0.85, 0.8, 0.75, 0.7, 0.65, 0.6, 0.55, 0.5]:
        #     for maxDelay in [1, 2, 4, 6, 8, 10, 20, 40, 80, 160, 320]:
        for threshold in [1.0, 0.9, 0.8, 0.7, 0.6, 0.5]:
            for maxDelay in [1, 2, 4, 8, 16, 32, 64, 128, 254, 512]:
                configurations.append(
                    Configuration.create(simulationName="THR:{:04.2f}-MD:{:02d}".format(threshold, maxDelay),
                                         resulsDirectory=resultsDir,
                                         plot=False,
                                         startId=startId,
                                         quantity=quantity,
                                         hostQuantity=hostNumber,
                                         hostProtoDelayType=HostProtoDelayType.LINEAR,
                                         hostProtoDelayConstDelay=10,
                                         hostProtoDelayMaxDelay=maxDelay,
                                         hostProtoDelayThreshold=threshold,
                                         hostProtoDelayMinDelay=0,
                                         hostProtoDelayBase=100,
                                         hostCommDelayType=HostCommDelayType.DEFAULT,
                                         beta=1,
                                         destinationOnlyFlag=df,
                                         scenarioType=ScenarioType.ADJUSTABLE,
                                         scenarioRatio=0.5,  # 0 means all hosts have PDR=1
                                         scenarioPdrMin=0,
                                         scenarioPdrMax=1,
                                         ))
        self.runConfigurations(configurations)

    def testExponentialDelay(self, df):
        # print("To run me, comment out 'return' on the next line...")
        # return

        if df:
            DF = "DFT"
        else:
            DF = "DFF"
        resultsDir = "R-Exponential-delay-{}".format(DF)
        hostNumber = 100
        quantity = 100
        startId = 100

        configurations = []
        maxDelay = None
        base = None
        configurations.append(Configuration.create(simulationName="Control",
                                                   resulsDirectory=resultsDir,
                                                   plot=True,
                                                   startId=startId,
                                                   quantity=quantity,
                                                   hostQuantity=hostNumber,
                                                   hostProtoDelayType=HostProtoDelayType.DEFAULT,
                                                   hostProtoDelayConstDelay=10,
                                                   hostProtoDelayMaxDelay=maxDelay,
                                                   hostProtoDelayThreshold=1,
                                                   hostProtoDelayMinDelay=0,
                                                   hostProtoDelayBase=base,
                                                   hostCommDelayType=HostCommDelayType.DEFAULT,
                                                   beta=1,
                                                   destinationOnlyFlag=df,
                                                   scenarioType=ScenarioType.ADJUSTABLE,
                                                   scenarioRatio=0.5,  # 0 means all hosts have PDR=1
                                                   scenarioPdrMin=0,
                                                   scenarioPdrMax=1,


                                                   ))
        self.runConfigurations(configurations)
        # return

        configurations = []
        # for base in [2]:  #, 4, 8, 16, 32, 64, 128, 256, 512, 1024]:  # 1.0, 0.95, 0.9, 0.85, 0.8, 0.75, 0.7, 0.65, 0.6, 0.55, 0.5]:
        #     for maxDelay in [40]:  # [1, 2, 4, 6, 8, 10, 20, 40, 80, 160, 320]:
        for base in [2, 4, 8, 16, 32, 64, 128, 256, 512, 1024]:
            for maxDelay in [1, 2, 4, 8, 16, 32, 64, 128, 254, 512]:
                configurations.append(
                    Configuration.create(simulationName="BASE:{:04d}-MD:{:03d}".format(base, maxDelay),
                                         resulsDirectory=resultsDir,
                                         plot=False,
                                         startId=startId,
                                         quantity=quantity,
                                         hostQuantity=hostNumber,
                                         hostProtoDelayType=HostProtoDelayType.EXPONENTIAL,
                                         hostProtoDelayConstDelay=10,
                                         hostProtoDelayMaxDelay=maxDelay,
                                         hostProtoDelayThreshold=1,
                                         hostProtoDelayMinDelay=0,
                                         hostProtoDelayBase=base,
                                         hostCommDelayType=HostCommDelayType.DEFAULT,
                                         beta=1,
                                         destinationOnlyFlag=df,
                                         scenarioType=ScenarioType.ADJUSTABLE,
                                         scenarioRatio=0.5,  # 0 means all hosts have PDR=1
                                         scenarioPdrMin=0,
                                         scenarioPdrMax=1,
                                         ))
        self.runConfigurations(configurations)

    def testNetworkDiameter(self):
        configurations = []
        # for beta in range(10, 21):
        #     beta = beta/10
        for hostQuantity in [60, 80, 100, 120, 140, 160, 180, 200, 220, 240, 260, 280, 300]:
            configurations.append(
                Configuration.create(simulationName="HostNumber: {:03d}".format(hostQuantity),
                                     resulsDirectory="R-Network_diameter_study-hostQuantity",
                                     plot=True,
                                     startId=100,
                                     quantity=100,
                                     hostQuantity=hostQuantity,
                                     hostProtoDelayType=HostProtoDelayType.DEFAULT,
                                     hostProtoDelayConstDelay=None,
                                     hostProtoDelayMaxDelay=None,
                                     hostProtoDelayThreshold=None,
                                     hostProtoDelayMinDelay=None,
                                     hostProtoDelayBase=None,
                                     hostCommDelayType=HostCommDelayType.DEFAULT,
                                     beta=1,
                                     destinationOnlyFlag=True,
                                     scenarioType=ScenarioType.NETWORK_DIAMETER,
                                     scenarioRatio=0,  # 0 means all hosts have PDR=1
                                     scenarioPdrMin=0,
                                     scenarioPdrMax=1,
                                     ))
        self.runConfigurations(configurations)

    def testLinearDelayZoomed(self):
        # print("To run me, comment out 'return' on the next line...")
        # return

        resultsDir = "R-Linear-zoomed"
        hostNumber = 100
        quantity = 100
        startId = 100

        configurations = []
        maxDelay = None
        threshold = None
        configurations.append(Configuration.create(simulationName="Control",
                                                   resulsDirectory=resultsDir,
                                                   plot=True,
                                                   startId=startId,
                                                   quantity=quantity,
                                                   hostQuantity=hostNumber,
                                                   hostProtoDelayType=HostProtoDelayType.DEFAULT,
                                                   hostProtoDelayConstDelay=10,
                                                   hostProtoDelayMaxDelay=maxDelay,
                                                   hostProtoDelayThreshold=threshold,
                                                   hostProtoDelayMinDelay=0,
                                                   hostProtoDelayBase=100,
                                                   hostCommDelayType=HostCommDelayType.DEFAULT,
                                                   beta=1,
                                                   destinationOnlyFlag=True,
                                                   scenarioType=ScenarioType.ADJUSTABLE,
                                                   scenarioRatio=0.5,  # 0 means all hosts have PDR=1
                                                   scenarioPdrMin=0,
                                                   scenarioPdrMax=1,


                                                   ))
        self.runConfigurations(configurations)
        # return

        configurations = []
        for threshold in [1.0]:  # 1.0, 0.95, 0.9, 0.85, 0.8, 0.75, 0.7, 0.65, 0.6, 0.55, 0.5]:
            for maxDelay in range(26, 40):  # [1, 2, 4, 6, 8, 10, 20, 40]:
                configurations.append(
                    Configuration.create(simulationName="THR:{:04.2f}-MD:{:02d}".format(threshold, maxDelay),
                                         resulsDirectory=resultsDir,
                                         plot=True,
                                         startId=startId,
                                         quantity=quantity,
                                         hostQuantity=hostNumber,
                                         hostProtoDelayType=HostProtoDelayType.LINEAR,
                                         hostProtoDelayConstDelay=10,
                                         hostProtoDelayMaxDelay=maxDelay,
                                         hostProtoDelayThreshold=threshold,
                                         hostProtoDelayMinDelay=0,
                                         hostProtoDelayBase=100,
                                         hostCommDelayType=HostCommDelayType.DEFAULT,
                                         beta=1,
                                         destinationOnlyFlag=True,
                                         scenarioType=ScenarioType.ADJUSTABLE,
                                         scenarioRatio=0.5,  # 0 means all hosts have PDR=1
                                         scenarioPdrMin=0,
                                         scenarioPdrMax=1,
                                         ))
        self.runConfigurations(configurations)

    def testLinearDelayQuantityDependency(self, df):
        # print("To run me, comment out 'return' on the next line...")
        # return

        if df:
            DF = "DFT"
        else:
            DF = "DFF"
        resultsDir = "R-Linear-quantity-dependency-{}".format(DF)
        hostNumber = 100
        startId = 100

        threshold = None

        configurations = []
        for quantity in [60, 80, 100, 120, 140, 160, 180, 200, 220, 240, 260, 280, 300]:
            for maxDelay in [1, 2, 4, 8, 16, 32, 64, 128, 254, 512]:
                configurations.append(
                    Configuration.create(simulationName="QUA:{:04d}-MD:{:02d}".format(quantity, maxDelay),
                                         resulsDirectory=resultsDir,
                                         plot=False,
                                         startId=startId,
                                         quantity=quantity,
                                         hostQuantity=hostNumber,
                                         hostProtoDelayType=HostProtoDelayType.LINEAR,
                                         hostProtoDelayConstDelay=10,
                                         hostProtoDelayMaxDelay=maxDelay,
                                         hostProtoDelayThreshold=1,
                                         hostProtoDelayMinDelay=0,
                                         hostProtoDelayBase=100,
                                         hostCommDelayType=HostCommDelayType.DEFAULT,
                                         beta=1,
                                         destinationOnlyFlag=df,
                                         scenarioType=ScenarioType.ADJUSTABLE,
                                         scenarioRatio=0.5,  # 0 means all hosts have PDR=1
                                         scenarioPdrMin=0,
                                         scenarioPdrMax=1,
                                         ))
        self.runConfigurations(configurations)

    def testExponentialDelayWithScenarios(self, DF):
        # print("To run me, comment out 'return' on the next line...")
        # return


        hostNumber = 100
        quantity = 100
        startId = 100

        # return
        if DF == True:
            DFname = "DFT"
        else:
            DFname = "DFF"

        for scenarioRatio in [0.1, 0.2, 0.3, 0.4]:
            resultsDir = "R-Exponential-SC{}-{}".format(scenarioRatio, DFname)
            configurations = []
            maxDelay = None
            base = None
            configurations.append(Configuration.create(simulationName="Control",
                                                       resulsDirectory=resultsDir,
                                                       plot=False,
                                                       startId=startId,
                                                       quantity=quantity,
                                                       hostQuantity=hostNumber,
                                                       hostProtoDelayType=HostProtoDelayType.DEFAULT,
                                                       hostProtoDelayConstDelay=10,
                                                       hostProtoDelayMaxDelay=maxDelay,
                                                       hostProtoDelayThreshold=1,
                                                       hostProtoDelayMinDelay=0,
                                                       hostProtoDelayBase=base,
                                                       hostCommDelayType=HostCommDelayType.DEFAULT,
                                                       beta=1,
                                                       destinationOnlyFlag=DF,
                                                       scenarioType=ScenarioType.ADJUSTABLE,
                                                       scenarioRatio=scenarioRatio,  # 0 means all hosts have PDR=1
                                                       scenarioPdrMin=0,
                                                       scenarioPdrMax=1,

                                                       ))
            self.runConfigurations(configurations)
            configurations = []

            for base in [2, 4, 8, 16, 32, 64, 128, 256, 512, 1024]:  # 1.0, 0.95, 0.9, 0.85, 0.8, 0.75, 0.7, 0.65, 0.6, 0.55, 0.5]:
                for maxDelay in [1, 2, 4, 8, 16, 32, 64, 128, 254, 512]:
                    configurations.append(
                        Configuration.create(simulationName="BASE:{:04d}-MD:{:03d}".format(base, maxDelay),
                                             resulsDirectory=resultsDir,
                                             plot=False,
                                             startId=startId,
                                             quantity=quantity,
                                             hostQuantity=hostNumber,
                                             hostProtoDelayType=HostProtoDelayType.EXPONENTIAL,
                                             hostProtoDelayConstDelay=10,
                                             hostProtoDelayMaxDelay=maxDelay,
                                             hostProtoDelayThreshold=1,
                                             hostProtoDelayMinDelay=0,
                                             hostProtoDelayBase=base,
                                             hostCommDelayType=HostCommDelayType.DEFAULT,
                                             beta=1,
                                             destinationOnlyFlag=DF,
                                             scenarioType=ScenarioType.ADJUSTABLE,
                                             scenarioRatio=scenarioRatio,  # 0 means all hosts have PDR=1
                                             scenarioPdrMin=0,
                                             scenarioPdrMax=1,
                                             ))
            self.runConfigurations(configurations)

    def testExponentialDelayBeta(self, df):
        # print("To run me, comment out 'return' on the next line...")
        # return


        hostNumber = 100
        quantity = 100
        startId = 100

        if df:
            DF = "DFT"
        else:
            DF = "DFF"

        configurations = []
        for beta in [16]:  # [12, 14, 16, 18, 20]:
            beta = beta/10
            resultsDir = "R-Exponential-{}-Beta:{}".format(DF, beta)
            configurations.append(
                Configuration.create(simulationName="Control",
                                     resulsDirectory=resultsDir,
                                     plot=False,
                                     startId=startId,
                                     quantity=quantity,
                                     hostQuantity=hostNumber,
                                     hostProtoDelayType=HostProtoDelayType.DEFAULT,
                                     hostProtoDelayConstDelay=10,
                                     hostProtoDelayMaxDelay=None,
                                     hostProtoDelayThreshold=1,
                                     hostProtoDelayMinDelay=0,
                                     hostProtoDelayBase=None,
                                     hostCommDelayType=HostCommDelayType.DEFAULT,
                                     beta=beta,
                                     destinationOnlyFlag=df,
                                     scenarioType=ScenarioType.ADJUSTABLE,
                                     scenarioRatio=0.5,  # 0 means all hosts have PDR=1
                                     scenarioPdrMin=0,
                                     scenarioPdrMax=1,
                                     ))
            self.runConfigurations(configurations)
            # continue
            for base in [2, 4, 8, 16, 32, 64, 128, 256, 512, 1024]:  # 1.0, 0.95, 0.9, 0.85, 0.8, 0.75, 0.7, 0.65, 0.6, 0.55, 0.5]:
                for maxDelay in [1, 2, 4, 8, 16, 32, 64, 128, 254, 512]:
                    configurations.append(
                        Configuration.create(simulationName="BASE:{:04d}-MD:{:03d}".format(base, maxDelay),
                                             resulsDirectory=resultsDir,
                                             plot=False,
                                             startId=startId,
                                             quantity=quantity,
                                             hostQuantity=hostNumber,
                                             hostProtoDelayType=HostProtoDelayType.EXPONENTIAL,
                                             hostProtoDelayConstDelay=10,
                                             hostProtoDelayMaxDelay=maxDelay,
                                             hostProtoDelayThreshold=1,
                                             hostProtoDelayMinDelay=0,
                                             hostProtoDelayBase=base,
                                             hostCommDelayType=HostCommDelayType.DEFAULT,
                                             beta=beta,
                                             destinationOnlyFlag=df,
                                             scenarioType=ScenarioType.ADJUSTABLE,
                                             scenarioRatio=0.5,  # 0 means all hosts have PDR=1
                                             scenarioPdrMin=0,
                                             scenarioPdrMax=1,
                                             ))
        self.runConfigurations(configurations)

    def testNewDijkstra(self):
        configurations = []
        configurations.append(
            Configuration.create(simulationName="Control",
                                 resulsDirectory="R-New-Dijkstra",
                                 plot=True,
                                 startId=0,
                                 quantity=1,
                                 hostQuantity=7,
                                 hostProtoDelayType=HostProtoDelayType.LINEAR,
                                 hostProtoDelayConstDelay=10,
                                 hostProtoDelayMaxDelay=40,
                                 hostProtoDelayThreshold=1,
                                 hostProtoDelayMinDelay=0,
                                 hostProtoDelayBase=None,
                                 hostCommDelayType=HostCommDelayType.DEFAULT,
                                 beta=1,
                                 destinationOnlyFlag=True,
                                 scenarioType=ScenarioType.ADJUSTABLE,
                                 scenarioRatio=0,  # 0 means all hosts have PDR=1
                                 scenarioPdrMin=0,
                                 scenarioPdrMax=1,
                                 ))
        configurations[0].layoutType = LayoutType.CUSTOM1
        configurations[0].topologyType = TopologyType.CUSTOM1
        self.runConfigurations(configurations)

    def testCompareDijkstraVersions(self):
        configurations = []
        configurations.append(
            Configuration.create(simulationName="Control",
                                 resulsDirectory="R-Test-Dijkstra-new",
                                 plot=False,
                                 startId=0,
                                 quantity=1000,
                                 hostQuantity=100,
                                 hostProtoDelayType=HostProtoDelayType.LINEAR,
                                 hostProtoDelayConstDelay=None,
                                 hostProtoDelayMaxDelay=40,
                                 hostProtoDelayThreshold=1,
                                 hostProtoDelayMinDelay=0,
                                 hostProtoDelayBase=None,
                                 hostCommDelayType=HostCommDelayType.DEFAULT,
                                 beta=1,
                                 destinationOnlyFlag=True,
                                 scenarioType=ScenarioType.ADJUSTABLE,
                                 scenarioRatio=0,  # 0 means all hosts have PDR=1
                                 scenarioPdrMin=0,
                                 scenarioPdrMax=1,
                                 ))
        # configurations[0].layoutType = LayoutType.CUSTOM1
        # configurations[0].topologyType = TopologyType.CUSTOM1
        self.runConfigurations(configurations)


    def testPlot10Topologies(self):
        configurations = []
        configurations.append(
            Configuration.create(simulationName="DFT",
                                 resulsDirectory="R-Pictures-color-links-svg",
                                 plot=True,
                                 startId=0,
                                 quantity=10,
                                 hostQuantity=100,
                                 hostProtoDelayType=HostProtoDelayType.EXPONENTIAL,
                                 hostProtoDelayConstDelay=None,
                                 hostProtoDelayMaxDelay=512,
                                 hostProtoDelayThreshold=1,
                                 hostProtoDelayMinDelay=0,
                                 hostProtoDelayBase=8,
                                 hostCommDelayType=HostCommDelayType.DEFAULT,
                                 beta=1,
                                 destinationOnlyFlag=True,
                                 scenarioType=ScenarioType.ADJUSTABLE,
                                 scenarioRatio=0.5,  # 0 means all hosts have PDR=1
                                 scenarioPdrMin=0,
                                 scenarioPdrMax=1,
                                 ))
        self.runConfigurations(configurations)

        configurations = []
        configurations.append(
            Configuration.create(simulationName="DFF",
                                 resulsDirectory="R-Pictures-color-links-svg",
                                 plot=True,
                                 startId=0,
                                 quantity=10,
                                 hostQuantity=100,
                                 hostProtoDelayType=HostProtoDelayType.EXPONENTIAL,
                                 hostProtoDelayConstDelay=None,
                                 hostProtoDelayMaxDelay=512,
                                 hostProtoDelayThreshold=1,
                                 hostProtoDelayMinDelay=0,
                                 hostProtoDelayBase=8,
                                 hostCommDelayType=HostCommDelayType.DEFAULT,
                                 beta=1,
                                 destinationOnlyFlag=False,
                                 scenarioType=ScenarioType.ADJUSTABLE,
                                 scenarioRatio=0.5,  # 0 means all hosts have PDR=1
                                 scenarioPdrMin=0,
                                 scenarioPdrMax=1,
                                 ))
        self.runConfigurations(configurations)

    def testCreateTopologyPictures(self):
        configurations = []
        for beta in [1, 1.2, 1.4, 1.6, 1.8, 2.0]:
            configurations.append(
                Configuration.create(simulationName=f"Beta-{beta}",
                                     resulsDirectory="R-Pictures-beta-color-links-svg",
                                     plot=True,
                                     startId=0,
                                     quantity=10,
                                     hostQuantity=100,
                                     hostProtoDelayType=HostProtoDelayType.EXPONENTIAL,
                                     hostProtoDelayConstDelay=None,
                                     hostProtoDelayMaxDelay=512,
                                     hostProtoDelayThreshold=1,
                                     hostProtoDelayMinDelay=0,
                                     hostProtoDelayBase=8,
                                     hostCommDelayType=HostCommDelayType.DEFAULT,
                                     beta=beta,
                                     destinationOnlyFlag=True,
                                     scenarioType=ScenarioType.ADJUSTABLE,
                                     scenarioRatio=0,  # 0 means all hosts have PDR=1
                                     scenarioPdrMin=0,
                                     scenarioPdrMax=1,
                                     ))
            configurations[-1].routingType = RoutingType.DEFAULT
            configurations[-1].metricType = MetricType.DEFAULT
        self.runConfigurations(configurations)

    def testConstDelayDFT(self):
        self.testConstantDelay(True)

    def testConstDelayDFF(self):
        self.testConstantDelay(False)

    def testLinearDelayDFT(self):
        self.testLinearDelay(True)

    def testLinearDelayDFF(self):
        self.testLinearDelay(False)

    def testExponentialDelayDFT(self):
        self.testExponentialDelay(True)

    def testExponentialDelayDFF(self):
        self.testExponentialDelay(False)

    def testExponentialDelayWithScenariosDFF(self):
        self.testExponentialDelayWithScenarios(False)

    def testExponentialDelayWithScenariosDFT(self):
        self.testExponentialDelayWithScenarios(True)

    def testExponentialDelayBetaDFT(self):
        self.testExponentialDelayBeta(True)

    def testExponentialDelayBetaDFF(self):
        self.testExponentialDelayBeta(False)

    def testLinearDelayQuantityDependencyDFT(self):
        self.testLinearDelayQuantityDependency(True)

    def testLinearDelayQuantityDependencyDFF(self):
        self.testLinearDelayQuantityDependency(False)
