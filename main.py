# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import glob
import math
import os.path
import statistics
import sys
from abc import ABC
from cmath import sqrt
from enum import Enum
from os import environ
from random import Random
from statistics import median

import matplotlib.pyplot as plt
import json

from numpy import percentile


class LinkUnknownHostException:
    pass

# Function to convert decimal to hexadecimal
def decimalToHexa(n):
    # char array to store hexadecimal number
    hexaDecimalNumber = ['0'] * 100

    # hexadecimal's counter number array
    i = 0

    while (n != 0):

        #  remainder is stored in a temporary variable named __temp
        _temp = 0

        # Storing remainder in _temp variable.
        _temp = n % 16

        # Check if _temp < 10
        if (_temp < 10):
            hexaDecimalNumber[i] = chr(_temp + 48)
            i = i + 1

        else:
            hexaDecimalNumber[i] = chr(_temp + 55)
            i = i + 1

        n = int(n / 16)

    hexadecimalCode = ""
    if (i == 2):
        hexadecimalCode = hexadecimalCode + hexaDecimalNumber[0]
        hexadecimalCode = hexadecimalCode + hexaDecimalNumber[1]

    elif (i == 1):
        hexadecimalCode = "0"
        hexadecimalCode = hexadecimalCode + hexaDecimalNumber[0]

    elif (i == 0):
        hexadecimalCode = "00"

    # Return the equivalent of hexadecimal color code
    return hexadecimalCode

def RGBtoHexConverion(R, G, B):
    if ((R >= 0 and R <= 255) and
            (G >= 0 and G <= 255) and
            (B >= 0 and B <= 255)):

        hexadecimalCode = "#"
        hexadecimalCode = hexadecimalCode + decimalToHexa(R)
        hexadecimalCode = hexadecimalCode + decimalToHexa(G)
        hexadecimalCode = hexadecimalCode + decimalToHexa(B)
        return hexadecimalCode

    # If the hexadecimal color code does not exist, return -1
    else:
        return "-1"

# class LinkBothHostsAreSetException:
#     pass

# https://stackoverflow.com/questions/24481852/serialising-an-enum-member-to-json
# class EnumEncoder(json.JSONEncoder):
#     def default(self, obj):
#         if type(obj) in HostType.values()\
#                 or type(obj) in LinkType.values()\
#                 or type(obj) in LayoutType.values()\
#                 or type(obj) in TopologyType.values()\
#                 or type(obj) in RoutingType.values()\
#                 or type(obj) in MetricType.values()\
#                 or type(obj) in ScenarioType.values()\
#                 or type(obj) in HostRoutingType.values()\
#                 or type(obj) in HostProtoDelayType.values()\
#                 or type(obj) in HostCommDelayType.values():
#             return {"__enum__": str(obj)}
#         return json.JSONEncoder.default(self, obj)

class HostType(str, Enum):
    DEFAULT = "DEFAULT"
    AODV = "AODV"


class LinkType(str, Enum):
    DEFAULT = "DEFAULT"


class LayoutType(str, Enum):
    UNIFORM = "UNIFORM"
    CUSTOM1 = "CUSTOM1"
    CUSTOM2 = "CUSTOM2"


class TopologyType(str, Enum):
    CUSTOM1 = "CUSTOM1"
    FULL_MESH = "FULL_MESH"
    GABRIEL_GRAPH = "GABRIEL_GRAPH"
    BETA_SKELETON_CIRCLE_BASED = "BETA_SKELETON_CIRCLE_BASED"
    BETA_SKELETON_LUNE_BASED = "BETA_SKELETON_LUNE_BASED"
    BETA_SKELETON = "BETA_SKELETON"
    CUSTOM2 = "CUSTOM2"

class RoutingType(str, Enum):
    DEFAULT = "DEFAULT"
    AODV = "AODV"

class MetricType(str, Enum):
    DEFAULT = "DEFAULT"
    ALL_METRIC = "ALL_METRIC"

class InstanceType(str, Enum):
    DEFAULT = "DEFAULT"
    TEST1 = "TEST1"
    EXPERIMENT1 = "EXPERIMENT1"
    PDR_1_PROTO_DELAY = "PDR_1_PROTO_DELAY"

class ScenarioType(str, Enum):
    DEFAULT = "DEFAULT"
    # TEST = "TEST"
    # FIVE_BAD_HOSTS = "FIVE_BAD_HOSTS"
    # CUSTOM2 = "CUSTOM2"
    ADJUSTABLE = "ADJUSTABLE"
    SHOW_TOPOLOGY_PROPERTIES = "SHOW_TOPOLOGY_PROPERTIES"
    NETWORK_DIAMETER = "NETWORK_DIAMETER"

def getDistance(x1, y1, x2, y2):
    x = abs(x1 - x2)
    y = abs(y1 - y2)
    return sqrt(x**2 + y**2).real

class HostRoutingType(str, Enum):
    AODV = "AODV"

class HostRouting:
    pass

class AODVHostRouting(HostRouting):
    def __init__(self):
        self._routingTable = {}

    def addRoute(self, dstHost, path):
        self._routingTable[dstHost] = path

    def getRouteTo(self, dstHost):
        return self._routingTable[dstHost]

class HostProtoDelayType(str, Enum):
    DEFAULT = "DEFAULT"
    CONST = "CONST"
    LINEAR = "LINEAR"
    EXPONENTIAL = "EXPONENTIAL"

class HostProtoDelay:
    def getDelay(self, host, neighbor):
        return 0

class HostProtoDelayConfiguration:
    def __init__(self):
        self.constDelay = 2
        self.minDelay = 0
        self.maxDelay = 10
        self.threshold = None
        self.base = None

class Pdr1HostProtoDelay(HostProtoDelay):
    def __init__(self, configuration: HostProtoDelayConfiguration):
        if configuration.constDelay is None:
            raise Exception("Pdr1HostProtoDelay: Missing option 'constDelay'")
        self._constDelay = configuration.constDelay

    def getDelay(self, host, neighbor):
        if neighbor.getPDR() < 1:
            return self._constDelay
        return 0

class LinearHostProtoDelay(HostProtoDelay):
    def __init__(self, configuration: HostProtoDelayConfiguration):
        if configuration.maxDelay is None:
            raise Exception("LinearHostProtoDelay: Missing option 'maxDelay'")
        if configuration.minDelay is None:
            raise Exception("LinearHostProtoDelay: Missing option 'minDelay'")
        if configuration.threshold is None:
            raise Exception("LinearHostProtoDelay: Missing option 'threshold'")
        self._maxDelay = configuration.maxDelay
        self._minDelay = configuration.minDelay
        self._threshold = configuration.threshold

    def getDelay(self, host, neighbor):
        if neighbor.getPDR() < self._threshold:
            return (self._maxDelay - self._minDelay) * (1 - neighbor.getPDR()) + self._minDelay
        return 0

class ConstHostProtoDelay(HostProtoDelay):
    def __init__(self, configuration: HostProtoDelayConfiguration):
        if configuration.constDelay is None:
            raise Exception("ConstHostProtoDelay: Missing option 'constDelay'")
        self._constDelay = configuration.constDelay
        if configuration.threshold is None:
            raise Exception("ConstHostProtoDelay: Missing option 'threshold'")
        self._threshold = configuration.threshold

    def getDelay(self, host, neighbor):
        if neighbor.getPDR() < self._threshold:
            return self._constDelay
        return 0

class ExponentialHostProtoDelay(HostProtoDelay):
    def __init__(self, configuration: HostProtoDelayConfiguration):
        if configuration.threshold is None:
            raise Exception("ExponentialHostProtoDelay: Missing option 'threshold'")
        self._threshold = configuration.threshold
        if configuration.maxDelay is None:
            raise Exception("ExponentialHostProtoDelay: Missing option 'maxDelay'")
        self._maxDelay = configuration.maxDelay
        if configuration.minDelay is None:
            raise Exception("ExponentialHostProtoDelay: Missing option 'minDelay'")
        self._minDelay = configuration.minDelay
        if configuration.base is None:
            raise Exception("ExponentialHostProtoDelay: Missing option 'base'")
        self._base = configuration.base

    def getDelay(self, host, neighbor):
        if neighbor.getPDR() < self._threshold:
            return (self._maxDelay - self._minDelay) * self._calcDelayFactor(neighbor.getPDR()) + self._minDelay
        return 0

    def _calcDelayFactor(self, pdr):
        return (self._base**((self._threshold-pdr)/self._threshold) - 1) / (self._base - 1)

class HostCommDelayType(str, Enum):
    DEFAULT = "DEFAULT"
    PDR = "PDR"

class HostCommDelayConfiguration:
    def __init__(self):
        pass

class HostCommDelay:
    def __init__(self, configuration: HostCommDelayConfiguration):
        self._configuration = configuration
    def getDelay(self, host, neighbor):
        return 1

class PDRHostCommDelay(HostCommDelay):
    def __init__(self, configuration: HostCommDelayConfiguration,
                 seed: int):
        super(PDRHostCommDelay, self).__init__(configuration)
        self._random = Random()
        self._random.seed(seed)

    def getDelay(self, host, neighbor):
        if self._random.uniform(0, 1) > host.getPDR():
            return 1000
        return 1


class Host:
    _id = 0

    @classmethod
    def getGlobalId(cls):
        return cls._id

    @classmethod
    def resetId(cls):
        cls._id = 0


    def __init__(self, x, y,
                 hostRouting: HostRouting = None,
                 hostProtoDelay: HostProtoDelay = None,
                 hostCommDelay: HostCommDelay = None):
        self._links = []
        self._x = x
        self._y = y
        self._id = Host._id
        Host._id += 1
        self._paths = []
        self._pdr = 1.0
        self._routing = hostRouting
        self._commDelay = hostCommDelay
        self._protoDelay = hostProtoDelay

    def __repr__(self):
        return "Host {0}".format(self._id)

    def addLink(self, link):
        self._links.append(link)

    def removeLink(self, link):
        self._links.remove(link)

    def getLinks(self):
        return self._links

    def getLinkToNeighbor(self, neighbor):
        for link in self._links:
            if link.getOtherHost(self) is neighbor:
                return link
        pass

    def getX(self):
        return self._x

    def getY(self):
        return self._y

    def getNeighbors(self):
        neighbors = []
        for link in self.getLinks():
            neighbors.append(link.getOtherHost(self))
        return neighbors

    def getNeighborNumber(self):
        count = len(self.getLinks())
        return len(self.getLinks())

    def setPDR(self, pdrValue: float):
        self._pdr = pdrValue

    def getPDR(self):
        return self._pdr

    def getId(self):
        return self._id

    def addPath(self, path):
        # print("{0}: Appending path: {1}".format(self, path))
         self._paths.append(path)

    def getPaths(self):
        return self._paths

    def getNumberOfPaths(self):
        return len(self._paths)

    def getRetransmissionDelay(self):
        return 1

    def getDelay(self, neighbor):
        return self._protoDelay.getDelay(self, neighbor) + self._commDelay.getDelay(self, neighbor)
        #return self._protoDelay.getDelay(neighbor, self) + self._commDelay.getDelay(self, neighbor)

    def getRouting(self):
        return self._routing

    def getProtoDelay(self):
        return self._protoDelay



# class AODVHost(Host):
#     def __init__(self, x: float, y: float):
#         super(AODVHost, self).__init__(x, y)
#         self._routingTable = {}
#
#     def addRoute(self, dstHost: Host, path):
#         self._routingTable[dstHost] = path
#
#     def getRouteTo(self, dstHost: Host):
#         return self._routingTable[dstHost]





class HostFactory:
    def __init__(self,
                 seed: int = None,
                 hostRoutingType: HostRoutingType = None,
                 hostProtocolDelayType: HostProtoDelayType = HostProtoDelayType.DEFAULT,
                 hostProtocolDelayConfiguration: HostProtoDelayConfiguration = HostProtoDelayConfiguration(),
                 hostCommDelayType: HostCommDelayType = HostCommDelayType.DEFAULT,
                 hostCommDelayConfiguration: HostCommDelayConfiguration = HostCommDelayConfiguration()):
        self._type = type
        self._hostRoutingType = hostRoutingType
        self._hostProtocolDelayType = hostProtocolDelayType
        self._hostProtocolDelayConfiguration = hostProtocolDelayConfiguration
        self._hostCommDelayType = hostCommDelayType
        self._hostCommDelayConfiguration = hostCommDelayConfiguration
        self._seed = seed

    def create(self, x: float, y: float):
        hostRouting = None
        if self._hostRoutingType is HostRoutingType.AODV:
            hostRouting = AODVHostRouting()

        hostProtocolDelay = None
        if self._hostProtocolDelayType is HostProtoDelayType.DEFAULT:
            hostProtocolDelay = HostProtoDelay()
        elif self._hostProtocolDelayType is HostProtoDelayType.CONST:
            hostProtocolDelay = ConstHostProtoDelay(self._hostProtocolDelayConfiguration)
        elif self._hostProtocolDelayType is HostProtoDelayType.LINEAR:
            hostProtocolDelay = LinearHostProtoDelay(self._hostProtocolDelayConfiguration)
        elif self._hostProtocolDelayType is HostProtoDelayType.EXPONENTIAL:
            hostProtocolDelay = ExponentialHostProtoDelay(self._hostProtocolDelayConfiguration)

        hostCommDelay = None
        if self._hostCommDelayType is HostCommDelayType.DEFAULT:
            hostCommDelay = HostCommDelay(self._hostCommDelayConfiguration)
        elif self._hostCommDelayType is HostCommDelayType.PDR:
            hostCommDelay = PDRHostCommDelay(self._hostCommDelayConfiguration, self._seed + Host.getGlobalId())

        return Host(x, y, hostRouting, hostProtocolDelay, hostCommDelay)


class Link:

    def __init__(self, first=None, second=None):
        self._first: Host = first
        self._second: Host = second
        self._first.addLink(self)
        self._second.addLink(self)
        self._length = None
        self._midX = None
        self._midY = None
        self._xLength = None
        self._yLength = None
        self._halfLength = None
        self._paths = []

    def __repr__(self):
        return "Link {0}-{1}".format(self.getFirstHost().getId(), self.getSecondHost().getId())

    def getOtherHost(self, currentHost):
        if currentHost == self._first:
            return self._second
        elif currentHost == self._second:
            return self._first
        else:
            raise LinkUnknownHostException()

    def getFirstHost(self):
        return self._first

    def getSecondHost(self):
        return self._second

    def getXLength(self):
        if self._xLength is None:
            self._xLength = abs(self.getFirstHost().getX() - self.getSecondHost().getX())
        return self._xLength

    def getYLength(self):
        if self._yLength is None:
            self._yLength = abs(self.getFirstHost().getY() - self.getSecondHost().getY())
        return self._yLength

    def getLength(self):
        if self._length is None:
            self._length = sqrt(self.getXLength()**2 + self.getYLength()**2).real
        return self._length

    def getHalfLenght(self):
        if self._halfLength is None:
            self._halfLength = self.getLength() / 2
        return self._halfLength

    def getMiddleX(self):
        if self._midX == None:
            if self.getFirstHost().getX() < self.getSecondHost().getX():
                self._midX = self.getFirstHost().getX() + self.getXLength() / 2
            else:
                self._midX = self.getSecondHost().getX() + self.getXLength() / 2
        return self._midX

    def getMiddleY(self):
        if self._midY == None:
            if self.getFirstHost().getY() < self.getSecondHost().getY():
                self._midY = self.getFirstHost().getY() + self.getYLength() / 2
            else:
                self._midY = self.getSecondHost().getY() + self.getYLength() / 2
        return self._midY

    def appendPath(self, path):
        self._paths.append(path)

    def getNumberOfPaths(self):
        return len(self._paths)


class LinkFactory:
    def __init__(self, type: LinkType = LinkType.DEFAULT):
        self._type = type

    def create(self, firstHost: Host, secondHost: Host):
        if self._type is LinkType.DEFAULT:
            return Link(firstHost, secondHost)
        else:
            raise NotImplementedError

class Layout:
    def __init__(self, xSize, ySize):
        self._xSize = xSize
        self._ySize = ySize
        self._hosts = []

    def addHost(self, host):
        self._hosts.append(host)

    def getHosts(self):
        return self._hosts

    def getNumberOfHosts(self):
        return len(self._hosts)

    def getXSize(self):
        return self._xSize

    def getYSize(self):
        return self._ySize

    def plot(self, ax):
        # x = []
        # y = []
        # for host in self._hosts:
        #     x.append(host.getX())
        #     y.append(host.getY())
        # plt.style.use('seaborn-whitegrid')

        for host in self._hosts:
            x = [host.getX()]
            y = [host.getY()]
            if host.getPDR() < 1:
                color = RGBtoHexConverion(255, 0, 0)
                # color = RGBtoHexConverion(int(255 * (1 - host.getPDR())), 0, 0)
                size = 2 + 8 * (1 - host.getPDR())
            else:
                color = 'blue'
                size = 5
            ax.plot(x, y, 'o', color=color, markersize=size)


        # fig, ax = plt.subplots()
        # ax.plot(x, y, 'o', color='gray')
        # ax.plot(x, y, 'bo-')
        for host in self._hosts:
            if host.getPDR() < 1:
                label = '{0}-{1}'.format(host.getId(), host.getPDR())
                # label = '{0}-{2}-{1:5.2f}'.format(host.getId(),
                #                                   host.getDelay(host),
                #                                   host.getPDR())
            else:
                label = '{0}'.format(host.getId())
            ax.annotate(label,
                        xy=(host.getX(), host.getY()),
                        xytext=(-2, 2), ha='right',
                        textcoords='offset points',
                        color='gray',
                        # arrowprops=dict(arrowstyle='->', shrinkA=0)
                        )



class LayoutFactory:
    def __init__(self, seed: int = 0, hostNumber: int = 20, xSize: int = 100, ySize: int = 100, type: LayoutType = LayoutType.UNIFORM):
        self._xSize = xSize
        self._ySize = ySize
        self._hostQuantity = hostNumber
        self._type = type
        self._seed = seed
        self._random = Random()
        self._random.seed(self._seed)

    def create(self, hostFactory: HostFactory):
        Host.resetId()
        if self._type is LayoutType.UNIFORM:
            return self._createUniform(hostFactory)
        if self._type is LayoutType.CUSTOM1:
            return self._createCustom1(hostFactory)
        if self._type is LayoutType.CUSTOM2:
            return self._createCustom2(hostFactory)
        raise NotImplementedError

    def _createUniform(self, hostFactory: HostFactory):
        layout = Layout(self._xSize, self._ySize)
        for id in range(self._hostQuantity):
            layout.addHost(hostFactory.create(self._random.random() * self._xSize,
                                              self._random.random() * self._ySize))
        return layout

    def _createCustom1(self, hostFactory: HostFactory):
        layout = Layout(100, 100)
        layout.addHost(hostFactory.create(0, 1))  # 0
        layout.addHost(hostFactory.create(1, 0))  # 1
        layout.addHost(hostFactory.create(1, 1))  # 2
        layout.addHost(hostFactory.create(1, 2))  # 3
        layout.addHost(hostFactory.create(2, 0))  # 4
        layout.addHost(hostFactory.create(2, 1))  # 5
        layout.addHost(hostFactory.create(2, 2))  # 6
        pdr = 0.3
        for host in layout.getHosts():
            host.setPDR(pdr)
            pdr += 0.1
        return layout

    def _createCustom2(self, hostFactory: HostFactory):
        layout = Layout(100, 100)
        layout.addHost(hostFactory.create(0, 1))  # 0
        layout.addHost(hostFactory.create(2, 2))  # 1
        layout.addHost(hostFactory.create(2, 1))  # 2
        layout.addHost(hostFactory.create(2, 0))  # 3
        layout.addHost(hostFactory.create(1, 2))  # 4
        layout.addHost(hostFactory.create(1, 1))  # 5
        layout.addHost(hostFactory.create(1, 0))  # 6
        return layout



class TopologyConfiguration:
    def __init__(self):
        self.beta = 1

class Topology:
    def __init__(self, layout: Layout, linkFactory: LinkFactory):
        self._links = []
        self._layout = layout
        self._linkFactory = linkFactory

    def addLink(self, link: Link):
        self._links.append(link)

    def removeLink(self, link: Link):
        self._links.remove(link)
        link.getFirstHost().removeLink(link)
        link.getSecondHost().removeLink(link)


    def getLinks(self):
        return self._links

    def plot(self):
        # plt.clf()
        plt.style.use('seaborn-whitegrid')
        fig, ax = plt.subplots()
        maxPaths = 0
        for link in self._links:
            if link.getNumberOfPaths() > maxPaths:
                maxPaths = link.getNumberOfPaths()
        for link in self._links:
            x = [link.getFirstHost().getX(), link.getSecondHost().getX()]
            y = [link.getFirstHost().getY(), link.getSecondHost().getY()]
            # print("{0}: {1}".format(link, link.getNumberOfPaths()))
            if link.getNumberOfPaths() == 0:
                plt.plot(x, y, '#949494', linewidth=0.5)
            else:
                lineWidth = 10 * (link.getNumberOfPaths() / maxPaths)
                if lineWidth < 0.5:
                    lineWidth = 0.5
                plt.plot(x, y, 'k-', linewidth=lineWidth)
        self.getLayout().plot(ax)
        # plt.show()

    def getNumberOfLinks(self):
        return len(self._links)

    def getDensity(self):
        return self.getNumberOfLinks() / ((self.getLayout().getNumberOfHosts() * (self.getLayout().getNumberOfHosts() - 1)) /2)

    def getAverageVertexDegree(self): # average neighbor number
        sum = 0
        for host in self.getLayout().getHosts():
            sum += host.getNeighborNumber()
        return sum / self.getLayout().getNumberOfHosts()

    def getLayout(self):
        return self._layout

    def _calculateDistance(self, first: Host, second: Host):
        x = abs(first.getX() - second.getX())
        y = abs(first.getY() - second.getY())
        return math.sqrt(x**2 + y**2).real

class FullMeshTopology(Topology):
    def __init__(self, layout: Layout, linkFactory: LinkFactory):
        super(FullMeshTopology, self).__init__(layout, linkFactory)
        hosts = layout.getHosts()
        for i in range(len(hosts) - 1):
            for j in range(i + 1, len(hosts)):
                # print("{0}, {1}".format(i, j))
                link = linkFactory.create(hosts[i], hosts[j])
                self.addLink(link)

## https://en.wikipedia.org/wiki/Beta_skeleton
class BetaSkeletonCircleBaseTopology(FullMeshTopology):
    def __init__(self, layout: Layout, linkFactory: LinkFactory,
                 configuration: TopologyConfiguration):
        super(BetaSkeletonCircleBaseTopology, self).__init__(layout, linkFactory)
        self._beta = configuration.beta
        self._generateTopology()

    def _generateTopology(self):
        if self._beta < 1:
            theta = math.pi - math.asin(self._beta)
        else:
            theta = math.asin(1/self._beta)
        for link in list(self._links):
            for host in self.getLayout().getHosts():
                if host is not link.getFirstHost() and host is not link.getSecondHost():
                    angle = self._calculateAngle(link.getFirstHost(),
                                                 link.getSecondHost(),
                                                 host)
                    if angle > theta:
                        self.removeLink(link)
                        break

    # https://en.wikipedia.org/wiki/Law_of_cosines
    def _calculateAngle(self, A: Host, B: Host, C: Host):
        a = self._calculateDistance(B, C)
        b = self._calculateDistance(A, C)
        c = self._calculateDistance(A, B)
        t = (a**2 + b**2 - c**2)/(2*a*b)
        return math.acos((a**2 + b**2 - c**2)/(2*a*b))

# https://www.sciencedirect.com/science/article/pii/S0925772112001642
class BetaSkeletonLuneBasedTopology(FullMeshTopology):
    def __init__(self, layout: Layout, linkFactory: LinkFactory,
                 configuration: TopologyConfiguration):
        super(BetaSkeletonLuneBasedTopology, self).__init__(layout, linkFactory)
        self._beta = configuration.beta
        self._generateTopology()

    def _generateTopology(self):
        for link in list(self._links):
            dist = self._calculateDistance(link.getFirstHost(), link.getSecondHost())
            xDist = link.getSecondHost().getX() - link.getFirstHost().getX()
            yDist = link.getSecondHost().getY() - link.getFirstHost().getY()
            firstCenter = Host(link.getFirstHost().getX() + xDist * (self._beta/2),
                               link.getFirstHost().getY() + yDist * (self._beta/2))
            secondCenter = Host(link.getSecondHost().getX() - xDist * (self._beta/2),
                                link.getSecondHost().getY() - yDist * (self._beta/2))
            diameter = self._beta * dist / 2
            for host in self.getLayout().getHosts():
                if host is link.getFirstHost() or host is link.getSecondHost():
                    continue
                distFirst = self._calculateDistance(host, firstCenter)
                distSecond = self._calculateDistance(host, secondCenter)
                if distFirst < diameter and distSecond < diameter:
                    self.removeLink(link)
                    break


class BetaSkeletonTopology(Topology):
    def __init__(self, layout: Layout, linkFactory: LinkFactory,
                 configuration: TopologyConfiguration):
        super(BetaSkeletonTopology, self).__init__(layout, linkFactory)
        self._beta = configuration.beta
        if self._beta < 1:
            topology = BetaSkeletonCircleBaseTopology(layout, linkFactory, configuration)
        else:
            topology = BetaSkeletonLuneBasedTopology(layout, linkFactory, configuration)
        self._links = topology.getLinks()


class TopologyFactory:
    def __init__(self, type: TopologyType.GABRIEL_GRAPH):
        self._type = type

    def create(self, layout: Layout, linkFactory: LinkFactory,
               configuration: TopologyConfiguration = TopologyConfiguration()):
        if self._type is TopologyType.GABRIEL_GRAPH:
            return self._createGG(layout, linkFactory)
        elif self._type is TopologyType.FULL_MESH:
            return self._createFullMesh(layout, linkFactory)
        elif self._type is TopologyType.CUSTOM1:
            return self._createCustom1(layout, linkFactory)
        elif self._type is TopologyType.BETA_SKELETON_CIRCLE_BASED:
            return BetaSkeletonCircleBaseTopology(layout, linkFactory, configuration)
        elif self._type is TopologyType.BETA_SKELETON_LUNE_BASED:
            return BetaSkeletonLuneBasedTopology(layout, linkFactory, configuration)
        elif self._type is TopologyType.BETA_SKELETON:
            return BetaSkeletonTopology(layout, linkFactory, configuration)
        elif self._type is TopologyType.CUSTOM2:
            return self._createCustom2(layout, linkFactory)
        else:
            raise NotImplementedError

    def _createGG(self, layout: Layout, linkFactory: LinkFactory):
        topology = self._createFullMesh(layout, linkFactory)
        hosts = layout.getHosts()
        links = topology.getLinks()
        # iterating over copy of the list because the original list will be modified when iterating over
        for link in list(links):
            if self._conditionGG(link, hosts):
                topology.removeLink(link)
        return topology

    def _createFullMesh(self, layout: Layout, linkFactory: LinkFactory):
        topology = Topology(layout, linkFactory)
        hosts = layout.getHosts()
        for i in range(len(hosts) - 1):
            for j in range(i + 1, len(hosts)):
                # print("{0}, {1}".format(i, j))
                link = linkFactory.create(hosts[i], hosts[j])
                topology.addLink(link)
        return topology

    def _conditionGG(self, link: Link, hosts: list):
        for host in hosts:
            if host is link.getFirstHost() or host is link.getSecondHost():
                continue
            distanceFromLinkMiddle = getDistance(link.getMiddleX(), link.getMiddleY(), host.getX(), host.getY())
            if distanceFromLinkMiddle <= link.getHalfLenght():
                return True
        return False

    def _createCustom1(self, layout: Layout, linkFactory: LinkFactory):
        topology = Topology(layout, linkFactory)
        topology.addLink(linkFactory.create(layout.getHosts()[0], layout.getHosts()[1]))
        topology.addLink(linkFactory.create(layout.getHosts()[0], layout.getHosts()[2]))
        topology.addLink(linkFactory.create(layout.getHosts()[0], layout.getHosts()[3]))
        topology.addLink(linkFactory.create(layout.getHosts()[1], layout.getHosts()[4]))
        topology.addLink(linkFactory.create(layout.getHosts()[1], layout.getHosts()[5]))
        topology.addLink(linkFactory.create(layout.getHosts()[2], layout.getHosts()[4]))
        topology.addLink(linkFactory.create(layout.getHosts()[2], layout.getHosts()[5]))
        topology.addLink(linkFactory.create(layout.getHosts()[2], layout.getHosts()[6]))
        topology.addLink(linkFactory.create(layout.getHosts()[3], layout.getHosts()[5]))
        topology.addLink(linkFactory.create(layout.getHosts()[3], layout.getHosts()[6]))

        topology.addLink(linkFactory.create(layout.getHosts()[1], layout.getHosts()[2]))
        topology.addLink(linkFactory.create(layout.getHosts()[2], layout.getHosts()[3]))
        topology.addLink(linkFactory.create(layout.getHosts()[4], layout.getHosts()[5]))
        topology.addLink(linkFactory.create(layout.getHosts()[5], layout.getHosts()[6]))
        return topology

    def _createCustom2(self, layout: Layout, linkFactory: LinkFactory):
        topology = Topology(layout, linkFactory)
        topology.addLink(linkFactory.create(layout.getHosts()[0], layout.getHosts()[4]))
        topology.addLink(linkFactory.create(layout.getHosts()[0], layout.getHosts()[5]))
        topology.addLink(linkFactory.create(layout.getHosts()[0], layout.getHosts()[6]))
        topology.addLink(linkFactory.create(layout.getHosts()[4], layout.getHosts()[5]))
        topology.addLink(linkFactory.create(layout.getHosts()[5], layout.getHosts()[6]))
        topology.addLink(linkFactory.create(layout.getHosts()[4], layout.getHosts()[1]))
        topology.addLink(linkFactory.create(layout.getHosts()[4], layout.getHosts()[2]))
        topology.addLink(linkFactory.create(layout.getHosts()[5], layout.getHosts()[1]))
        topology.addLink(linkFactory.create(layout.getHosts()[5], layout.getHosts()[2]))
        topology.addLink(linkFactory.create(layout.getHosts()[5], layout.getHosts()[3]))
        topology.addLink(linkFactory.create(layout.getHosts()[6], layout.getHosts()[2]))
        topology.addLink(linkFactory.create(layout.getHosts()[6], layout.getHosts()[3]))
        topology.addLink(linkFactory.create(layout.getHosts()[1], layout.getHosts()[2]))
        topology.addLink(linkFactory.create(layout.getHosts()[2], layout.getHosts()[3]))
        return topology

class CommunicationModel(str, Enum):
    ALL_TO_ALL = "ALL_TO_ALL"
    SINK = "SINK"

class Routing:
    def __init__(self, topology: Topology,
                 communicationModel: CommunicationModel = CommunicationModel.ALL_TO_ALL,
                 sinkId: int = 0):
        self._topology: Topology = topology
        self._paths = []
        self._communicationModel = communicationModel
        self._sinkId = sinkId
        self._topologyDiameter = None

    def getTopology(self):
        return self._topology

    def getTopologyDiameter(self):
        if self._topologyDiameter is None:
            hosts = self.getTopology().getLayout().getHosts()
            maxPathLength = 0
            for host in hosts:
                paths = self.dijkstra(host)
                for key in paths:
                    length = paths[key].getLength()
                    if length > maxPathLength:
                        maxPathLength = length
            self._topologyDiameter = maxPathLength
        return self._topologyDiameter

    # def getTopoologyDiameter(self):
    #     if self._topologyDiameter is None:
    #         longestPathLength = 0
    #         numOfHosts = self.getTopology().getLayout().getNumberOfHosts()
    #         hosts = self.getTopology().getLayout().getHosts()
    #         for i in range(numOfHosts - 1):
    #             for j in range(i + 1, numOfHosts):
    #                 pathLength = self.findLengthOfShortestPath(hosts[i], hosts[j])
    #                 if pathLength > longestPathLength:
    #                     longestPathLength = pathLength
    #
    #         self._topologyDiameter = longestPathLength
    #     return self._topologyDiameter

    def dijkstra(self, srcHost: Host):
        queue = []
        dist = {}
        prev = {}

        hosts = self.getTopology().getLayout().getHosts()
        for host in hosts:
            dist[host] = 99999999
            prev[host] = None
            queue.append(host)
        dist[srcHost] = 0

        while len(queue) > 0:
            host: Host = self._getHostWithShortestDistance(dist, queue)
            queue.remove(host)
            for neighbor in host.getNeighbors():
                delay = host.getDelay(neighbor)
                alt = dist[host] + host.getDelay(neighbor)  # old
                if alt < dist[neighbor]:
                    dist[neighbor] = alt
                    prev[neighbor] = host

        paths = {}
        for host in hosts:
            path = Path(host)
            if prev[host] is None:
                continue
            h = host
            while prev[h] != None:
                h = prev[h]
                path.addHost(h)
            paths[host] = path

        return paths

    def _getHostWithShortestDistance(self, dist, queue):
        shortestHost = None
        shortestValue = 99999999
        for host in queue:
            if dist[host] < shortestValue:
                shortestValue = dist[host]
                shortestHost = host
        return shortestHost


    def appendPath(self, path):
        self._paths.append(path)
        for host in path.getHosts():
            host.addPath(path)
        hosts = path.getHosts()
        # print(path)
        for i in range(len(hosts) - 1):
            # linkToNeighbor = hosts[i].getLinkToNeighbor(hosts[i + 1])
            # print(">>{0} - {1}".format(hosts[i], hosts[i + 1]))
            hosts[i].getLinkToNeighbor(hosts[i + 1]).appendPath(path)


    def getPaths(self):
        return self._paths

    def generatePaths(self):
        # """
        # Generates the set of paths depending on the communication model.
        # """
        # hosts = self.getTopology().getLayout().getHosts()
        # if self._communicationModel == CommunicationModel.SINK:
        #     for host in hosts:
        #         if host.getId() != self._sinkId:
        #             path = self._generatePath(host, hosts[self._sinkId])
        #             self.appendPath(path)
        #             self.appendPath(path.getReversePath())
        # elif self._communicationModel == CommunicationModel.ALL_TO_ALL:
        #     for i in range(len(hosts) - 1):
        #         for j in range(i + 1, len(hosts)):
        #             path = self._generatePath(hosts[i], hosts[j])
        #             self.appendPath(path)
        #             self.appendPath(path.getReversePath())
        # else:
        pass
        # raise NotImplementedError


    def _generatePath(self, srcHost: Host, dstHost: Host):
        raise NotImplementedError

    def plot(self):
        self._topology.plot()
        # for path in self._paths:
        #     hosts = path.getHosts()
        #     for i in range(len(hosts) - 1):
        #         x = [hosts[i].getX(), hosts[i+1].getX()]
        #         y = [hosts[i].getY(), hosts[i+1].getY()]
        #         plt.plot(x, y, linewidth=10.0)

    def saveToFile(self, path):
        file = open(path, "w")
        for path in self._paths:
            file.write(repr(path))
            file.write(": {0}: ".format(path.getPDR()))
            for host in path.getIntermediateHosts():
                file.write("{0}-{1} ".format(host.getId(), host.getPDR()))
            file.write("\n")
        file.close()

class RoutingConfiguration:
    def __init__(self):
        self.sinkId = 0
        self.communicationModel = CommunicationModel.SINK
        self.destinationOnlyFlag = False


class AODVRouting(Routing):
    def __init__(self, topology: Topology,
                 params: RoutingConfiguration):
        self._destinationOnlyFlag = params.destinationOnlyFlag
        self._routes = {}
        super(AODVRouting, self).__init__(topology, params.communicationModel, params.sinkId)
        self._rddPaths = None

    def generatePaths(self):
        hosts = self.getTopology().getLayout().getHosts()
        if self._communicationModel is CommunicationModel.SINK:
            if not self._destinationOnlyFlag:
                self._generatePaths()
            else:
                paths = self.dijkstra(hosts[0])
                for host in hosts:
                    if host.getId() == self._sinkId:
                        continue
                    # print(paths[host])
                    # print(paths[host].getDelay())
                    self.appendPath(paths[host])
        else:
            raise NotImplementedError

    # If the destinationOnlyFlag disabled
    def _generatePaths(self):
        hosts = self.getTopology().getLayout().getHosts()
        # pathsToSink = self.dijkstra(hosts[self._sinkId])
        hostsWithRoute = []
        self._rddPaths = []  # For metric calculation
        # file = open("paths", "w")
        for host in hosts:
            if host.getId() == self._sinkId:
                continue
            if host in hostsWithRoute:
                continue
            paths = self.dijkstra(host)
            for h in paths:
                paths[h] = paths[h].getReversePath()
            #bestPath = pathsToSink[host]
            bestPath = paths[hosts[self._sinkId]]
            closestHost = None
            for hostWithRoute in hostsWithRoute:
                a = paths[hostWithRoute].getDelay()
                b = bestPath.getDelay()
                if a < b:
                    bestPath = paths[hostWithRoute]
                    closestHost = hostWithRoute
                elif a == b and closestHost is not None:
                    l1 = hostWithRoute.getRouting().getRouteTo(hosts[self._sinkId]).getLength()
                    l2 = closestHost.getRouting().getRouteTo(hosts[self._sinkId]).getLength()
                    if l1 < l2:
                        bestPath = paths[hostWithRoute]
                        closestHost = hostWithRoute
            # bestPath now contains the path to the destination or to the host with rout to the destination
            # The delay of the best path is the smallest one
            # file.write("{}\n".format(bestPath))
            self._rddPaths.append(bestPath.getDelay())  # only paths which causes searching delay are counted in
            # Now connect the path to the host with the route to the destination
            if bestPath.getDestinationHost().getId() != self._sinkId:
                bestPath = bestPath.removeHost(closestHost) + closestHost.getRouting().getRouteTo(hosts[self._sinkId])
            i = 0
            for intermediateHost in bestPath.getHosts()[:-1]:
                if intermediateHost in hostsWithRoute:
                    continue
                path = Path()
                for h in bestPath.getHosts()[i:]:
                    path.addHost(h)
                intermediateHost.getRouting().addRoute(hosts[self._sinkId], path)
                hostsWithRoute.append(intermediateHost)
                self.appendPath(path)
                i += 1
        # file.close()
            # h.addRoute(hosts[self._sinkId], bestPath)
            #hostsWithRoute.append(h)

    def getRDDValues(self):
        if self._rddPaths is None:
            raise Exception("AODVRouting::getDRRValues(): Values are not calculated, call method '_generatePaths' first.")
        return self._rddPaths

    def getDF(self):
        return self._destinationOnlyFlag

class RoutingFactory:

    def __init__(self, routingType = None, params = None):
        self._type = routingType
        self._params = params

    def create(self, topology: Topology):
        if self._type is RoutingType.AODV:
            return AODVRouting(topology, self._params)
        elif self._type is RoutingType.DEFAULT:
            return Routing(topology)

class Path:
    def __init__(self, host:Host = None):
        if host != None:
            self._hosts = [host]
        else:
            self._hosts = []

    def __repr__(self):
        repr = "{0}".format(self._hosts[0].getId())
        for host in self._hosts[1:]:
            repr += "-{0}".format(host.getId())
        return repr

    def __eq__(self, other):
        for i in len(self._hosts):
            if self._hosts[i] is not other._hosts[i]:
                return False
        return True

    def __ne__(self, other):
        for i in len(self._hosts):
            if self._hosts[i] is not other._hosts[i]:
                return True
        return False


    def getNumberOfHops(self):
        return len(self._hosts) - 1

    def getPDR(self):
        pdr = 1
        for host in self._hosts[1:-1]:
            pdr *= host.getPDR()
        return pdr

    def getHosts(self):
        return self._hosts

    def isHostInPath(self, host):
        return host in self._hosts

    def isHostIntemediate(self, host):
        return host in self._hosts[1:-1]

    def getSourceHost(self):
        return self._hosts[0]

    def addHost(self, host: Host):
        self._hosts.append(host)
        return self

    def removeHost(self, host: Host):
        self._hosts.remove(host)
        return self

    def copy(self):
        path = Path()
        for host in self._hosts:
            path.addHost(host)
        return path

    def copyAndAppend(self, host: Host):
        path = self.copy()
        path.addHost(host)
        return path

    def getSourceHost(self):
        return self._hosts[0]

    def getDestinationHost(self):
        return self._hosts[-1]

    def getIntermediateHosts(self):
        return self._hosts[1:-1]

    def getReversePath(self):
        path = Path()
        i = len(self._hosts) - 1
        while i >= 0:
            path.addHost(self._hosts[i])
            i -= 1
        return path

    def getLength(self):
        return self.getNumberOfHops()

    def __add__(self, other):
        path = self.copy()
        for host in other.getHosts():
            path.addHost(host)
        return path

    def getDelay(self):
        hosts = self._hosts
        delay = 0
        for i in range(len(hosts) - 1):
            delay += hosts[i].getDelay(hosts[i + 1])
        return delay

class Metric:

    def __init__(self, routing: Routing):
        self._routing = routing

    def getRouting(self):
        return self._routing

    def calculateAndLog(self):
        raise NotImplementedError

    def calculate(self, result):
        return 0

    def plot(self):
        self._routing.plot()

    def getHeader(self):
        return []


class AllMetric(Metric):
    def __init__(self, routing: Routing):
        super(AllMetric, self).__init__(routing)

    def calculate(self, result):
        valuesPDR = []  # path PDRs
        valuesPL = []  # path lengths
        valuesRDD = []  # path delay

        if self._routing.__class__ is AODVRouting:
            DF = self._routing.getDF()
            if DF is False:
                valuesRDD = self._routing.getRDDValues()

        for path in self.getRouting().getPaths():
            valuesPDR.append(path.getPDR())
            valuesPL.append(path.getLength())
            if DF is True:
                valuesRDD.append(path.getDelay())
        # print(paths)
        # print(values)

        result.PDRavg = statistics.mean(valuesPDR)
        result.PDRmed = statistics.median(valuesPDR)
        result.PDRqu1 = percentile(valuesPDR, 25)
        result.PDRqu3 = percentile(valuesPDR, 75)
        result.PDRmin = min(valuesPDR)
        result.PDRmax = max(valuesPDR)

        result.PLavg = statistics.mean(valuesPL)
        result.PLmed = statistics.median(valuesPL)
        result.PLqu1 = percentile(valuesPL, 25)
        result.PLqu3 = percentile(valuesPL, 75)
        result.PLmin = min(valuesPL)
        result.PLmax = max(valuesPL)

        result.RDDavg = statistics.mean(valuesRDD)
        result.RDDmed = statistics.median(valuesRDD)
        result.RDDqu1 = percentile(valuesRDD, 25)
        result.RDDqu3 = percentile(valuesRDD, 75)
        result.RDDmin = min(valuesRDD)
        result.RDDmax = max(valuesRDD)


    def getHeader(self):
        return ["PDRavg", "PDRmed", "PDRqu1", "PDRqu3", "PDRmin", "PDRmax",
                "PLavg", "PLmed", "PLqu1", "PLqu3", "PLmin", "PLmax",
                "RDDavg", "RDDmed", "RDDqu1", "RDDqu3", "RDDmin", "RDDmax"]

class MetricFactory:


    def __init__(self, type: MetricType):
        self._type = type

    def create(self, routing: Routing):
        if self._type is MetricType.ALL_METRIC:
            return AllMetric(routing)
        elif self._type is MetricType.DEFAULT:
            return Metric(routing)
        else:
            raise NotImplementedError

class Result:

    def getItems(self):
        items = ""
        for key in self.__dict__:
            items += "{0}\t".format(key)
        return items

    def getValues(self):
        values = ""
        for key in self.__dict__:
            if key == "ID":
                values += "{:04}\t".format(self.__dict__[key])
            else:
                values += "{:0.4f}\t".format(self.__dict__[key])
        return values


class Results:
    def __init__(self):
        self._results = {}
        self._statsData = None
        self._statsDataText = None

    def store(self, instanceId: int,  result: Result):
        self._results[instanceId] = result

    def load(self, instanceId: int):
        return self._results[instanceId]

    def isEmpty(self):
        return len(self._results) == 0

    def _prepareStatsData(self):
        if self._statsData is None:
            statsData = {}
            statsDataText = ""
            firstResultKey = list(self._results.keys())[0]
            for metric in self._results[firstResultKey].__dict__.keys():
                if metric == "ID":
                    continue
                statsData[metric] = []
            for key in self._results:
                for metric in self._results[key].__dict__.keys():
                    if metric == "ID":
                        continue
                    statsData[metric].append(self._results[key].__dict__[metric])
            self._statsData = statsData
            # Average
            statsDataText += "AVG:\t"
            for key in self._statsData:
                statsDataText += "{:0.4f}\t".format(statistics.mean(self._statsData[key]))
            statsDataText += "\n"
            # Median
            statsDataText += "MED:\t"
            for key in self._statsData:
                statsDataText += "{:0.4f}\t".format(statistics.median(self._statsData[key]))
            statsDataText += "\n"
            # first quartile
            statsDataText += "QT1:\t"
            for key in self._statsData:
                statsDataText += "{:0.4f}\t".format(percentile((self._statsData[key]), 25))
            statsDataText += "\n"
            # Third quartile
            statsDataText += "QT3:\t"
            for key in self._statsData:
                statsDataText += "{:0.4f}\t".format(percentile((self._statsData[key]), 75))
            statsDataText += "\n"
            # Min
            statsDataText += "MIN:\t"
            for key in self._statsData:
                statsDataText += "{:0.4f}\t".format(min(self._statsData[key]))
            statsDataText += "\n"
            # Max
            statsDataText += "MAX:\t"
            for key in self._statsData:
                statsDataText += "{:0.4f}\t".format(max(self._statsData[key]))
            statsDataText += "\n"
            self._statsDataText = statsDataText
        return self._statsData

    def saveToFile(self, path):
        if self.isEmpty():
            return
        # path = "{0}/{1}".format(configuration.resultsDirectory, configuration.simulationName)
        # if not os.path.exists(path):
        #     os.makedirs(path)
        file = open(path + "/results", "w")
        firstId = list(self._results.keys())[0]
        file.write(self._results[firstId].getItems())
        file.write("\n")

        for key in self._results:
            file.write(self._results[key].getValues())
            file.write("\n")
        self._prepareStatsData()

        file.write(self._statsDataText)

        file.close()

    def getStatsText(self):
        if self._statsDataText is None:
            self._prepareStatsData()
        return self._statsDataText

    def getStats(self):
        if self._statsData is None:
            self._prepareStatsData()
        return self._statsData

    def getResult(self, id: int):
        return self._results[id]

    def getResults(self):
        return self._results.values()



    def _analyze(self, values):
        values.sort()
        min = values[0]
        max = values[-1]
        med = statistics.median(values)
        avg = statistics.mean(values)
        return avg, min, max, med

class ScenarioConfiguration:
    def __init__(self):
        self.ratio = 0.05 # How many hosts will be affected
        self.pdrMin = 0.8
        self.pdrMax = 0.8



class Scenario:

    def __init__(self, metric: Metric, id: int, results: Results):
        self._metric = metric
        self._id = id
        self._results: Results = results

    def getMetric(self):
        return self._metric

    def execute(self, silent=True):
        raise NotImplementedError

    def getId(self):
        if self._id is None:
            raise NotImplementedError
        return self._id

    def getHosts(self):
        return self.getMetric().getRouting().getTopology().getLayout().getHosts()

    def getLinks(self):
        return self.getMetric().getRouting().getTopology().getLinks()

    def getPaths(self):
        return self.getMetric().getRouting().getPaths()

    # def setId(self, id: int):
    #     self._id = id
    #
    # def setResults(self, results: Results):
    #     self._results = results

    def plot(self):
        plt.close('all')
        self._metric.plot()

    def show(self):
        self.plot()
        plt.show()

    def printHeader(self):
        raise NotImplementedError

    def printFinalStats(self):
        raise NotImplementedError

    def getHeader(self):
        raise NotImplementedError

# class TestScenario(Scenario):
#     def __init__(self, metric: Metric, id: int, results: Results):
#         super(TestScenario, self).__init__(metric, id , results)
#
#     def execute(self, silent=False):
#         hosts = self.getMetric().getRouting().getTopology().getLayout().getHosts()
#         hosts[2].setPDR(0.5)
#         self.getMetric().getRouting().generatePaths()
#         self.plot()
#         plt.savefig("Results/{0}.png".format(self.getId()))
#         self._results.store(self.getId(), self.getMetric().calculate())
#         Logger.log("\tInstance {0}: {1}".format(self.getId(), self._results.load(self.getId())))
#
# class FiveBadHostsScenario(Scenario):
#     def __init__(self, metric: Metric, id: int, results: Results):
#         super(FiveBadHostsScenario, self).__init__(metric, id, results)
#
#     def execute(self, silent=False):
#         hosts = self.getMetric().getRouting().getTopology().getLayout().getHosts()
#         for i in range(1, 6):
#             hosts[i].setPDR(0.8)
#         self.getMetric().getRouting().generatePaths()
#         self._results.store(self.getId(), self.getMetric().calculate())
#         if not silent:
#             Logger.log("\tInstance {0}: {1}".format(self.getId(), self._results.load(self.getId())))
#
# class Custom2Scenario(Scenario):
#     def __init__(self, metric: Metric, id: int, results: Results):
#         super(Custom2Scenario, self).__init__(metric, id, results)
#
#     def execute(self, silent=False):
#         hosts = self.getHosts()
#         hosts[4].setPDR(0.5)
#         self.getMetric().getRouting().generatePaths()
#         # self.plot()
#         self.show()

class AdjustableScenario(Scenario):
    def __init__(self, metric: Metric, id: int, results: Results, configuration: ScenarioConfiguration):
        super(AdjustableScenario, self).__init__(metric, id, results)
        self._configuration = configuration
        if configuration.ratio is None or configuration.pdrMax is None or configuration.pdrMin is None:
            raise Exception("AdjustableScenario: Missing some of options 'ratio', 'pdrMin' or 'pdrMax'")

    def execute(self, silent=False):
        result = Result()
        random = Random()
        random.seed(self.getId())

        # setting PDRs regarding the parameters
        quantity = int(round(len(self.getHosts()) * self._configuration.ratio, 0))
        if quantity != 0:
            i = len(self.getHosts()) - 1
            modified = 0
            while modified < quantity:
                self.getHosts()[i].setPDR(round(random.uniform(self._configuration.pdrMin,
                                                         self._configuration.pdrMax),
                                                2))
                modified += 1
                i -= 1

        self.getMetric().getRouting().generatePaths()

        result.ID = self.getId()
        result.netDen = self.getMetric().getRouting().getTopology().getDensity()
        result.netAVD = self.getMetric().getRouting().getTopology().getAverageVertexDegree()
        self.getMetric().calculate(result)

        self._results.store(self.getId(), result)
        # if not silent:
        #     Logger.log("\t{0}\t{1}".format(result.ID, result.networkPDR))

    def printHeader(self):
        pass

    def getHeader(self):
        header = []
        header.append("ID")
        header.append("netDen")
        header.append("netAVD")

        metricHeader = self._metric.getHeader()
        for item in metricHeader:
            header.append(item)
        return header

    def printFinalStats(self, results):
        pass
        # sum = 0
        # for result in results.getResults():
        #     sum += result.networkPDR
        # Logger.log("\tAverage network PDR: {0}".format(sum / len(results.getResults())))

class ShowTopologyPropertiesScenario(Scenario):
    def __init__(self, metric: Metric, id: int, results: Results, configuration: ScenarioConfiguration):
        super(ShowTopologyPropertiesScenario, self).__init__(metric, id, results)

    def execute(self, silent=True):
        topology = self.getMetric().getRouting().getTopology()
        result = Result()
        result.ID = self.getId()
        result.dens = topology.getDensity()
        result.AVD = topology.getAverageVertexDegree()
        # topology.get
        self._results.store(self.getId(), result)

    def printHeader(self):
        print("Header")

    def getHeader(self):
        return ["", "Density", "AVD"]

    def printFinalStats(self):
        print("Final stats")

class NetworkDiameterScenario(Scenario):
    def __init__(self, metric: Metric, id: int, results: Results):
        super(NetworkDiameterScenario, self).__init__(metric, id, results)

    def execute(self, silent=True):
        result = Result()
        result.ID = self.getId()
        result.diam = self.getMetric().getRouting().getTopologyDiameter()
        self._results.store(self.getId(), result)

    def printHeader(self):
        print("Header")

    def getHeader(self):
        return ["", "Diam"]

    def printFinalStats(self):
        print("Final stats")

class ScenarioFactory:

    def __init__(self, type):
        self._type = type

    def create(self, metric: Metric, id: int, results: Results, configuration: ScenarioConfiguration):
        if self._type is ScenarioType.DEFAULT:
            return Scenario(metric, id, results)
        # elif self._type is ScenarioType.TEST:
        #     return TestScenario(metric, id, results)
        # elif self._type is ScenarioType.FIVE_BAD_HOSTS:
        #     return FiveBadHostsScenario(metric, id, results)
        # elif self._type is ScenarioType.CUSTOM2:
        #     return Custom2Scenario(metric, id, results)
        elif self._type is ScenarioType.ADJUSTABLE:
            return AdjustableScenario(metric, id, results, configuration)
        elif self._type is ScenarioType.SHOW_TOPOLOGY_PROPERTIES:
            return ShowTopologyPropertiesScenario(metric, id, results, configuration)
        elif self._type is ScenarioType.NETWORK_DIAMETER:
            return NetworkDiameterScenario(metric, id, results)
        else:
            raise NotImplementedError

# class Instance:
#
#     def __init__(self, scenario: Scenario, id: int, results):
#         self._scenario = scenario
#         self._scenario.setResults(results)
#         self._scenario.setId(id)
#         self._results = results
#
#     def run(self):
#         self._scenario.execute()
#
#     def getResults(self):
#         return self._results
#
#     def getId(self):
#         return self._scenario.getId()

class Configuration:
    def __init__(self):
        self.simulationName = None
        self.resultsDirectory = None
        self.plot = None
        self.startId = None
        self.quantity = None
        self.seedId = None
        self.xSize = None
        self.ySize = None
        self.hostQuantity = None
        self.hostRoutingType = None
        self.hostProtoDelayType = None
        self.hostProtoDelayConfiguration = HostProtoDelayConfiguration()
        self.hostCommDelayType = None
        self.hostCommDelayConfiguration = HostCommDelayConfiguration()
        self.layoutType = None
        self.linkType = None
        self.topologyType = None
        self.topologyConfiguration = TopologyConfiguration()

        self.routingType = None
        self.communicationModel = None
        self.routingConfiguration = RoutingConfiguration()
        self.routingConfiguration.sinkId = None
        self.routingConfiguration.communicationModel = None
        self.routingConfiguration.destinationOnlyFlag = False


        self.metricType = None
        self.scenarioType = None
        self.scenarioConfiguration = ScenarioConfiguration()
        self.results = None

    def saveToFile(self):
        path = "{0}/{1}".format(self.resultsDirectory, self.simulationName)
        if not os.path.exists(path):
            os.makedirs(path)
        jsonString = json.dumps(self,
                                default=lambda o: o.__dict__,
                                indent=4
                                )
        file = open(path + "/configuration.json", "w")
        file.write(jsonString)
        file.close()

    @classmethod
    def create(cls,
              simulationName: str = None,
              resulsDirectory: str = None,
              plot: bool = True,
               startId: int = None,
               quantity: int = None,
              hostQuantity: int = 20,
              hostProtoDelayType: HostProtoDelayType = None,
              hostProtoDelayConstDelay: float = None,
              hostProtoDelayMinDelay: float = None,
              hostProtoDelayMaxDelay: float = None,
              hostProtoDelayThreshold: float = None,
              hostProtoDelayBase: float = None,
              hostCommDelayType: HostCommDelayType = None,
              beta: float = None,
              destinationOnlyFlag: bool = None,
              scenarioType: ScenarioType = ScenarioType.ADJUSTABLE,
              scenarioRatio: float = None,
              scenarioPdrMin: float = None,
              scenarioPdrMax: float = None,
               ):
        configuration = Configuration()
        configuration.simulationName = simulationName
        configuration.resultsDirectory = resulsDirectory
        configuration.plot = plot
        configuration.startId = startId
        configuration.quantity = quantity
        configuration.seedId = None
        configuration.xSize = 100
        configuration.ySize = 100
        configuration.hostQuantity = hostQuantity

        configuration.hostRoutingType = HostRoutingType.AODV
        configuration.hostProtoDelayType = hostProtoDelayType  # the default proto delay is 0
        configuration.hostProtoDelayConfiguration.constDelay = hostProtoDelayConstDelay
        configuration.hostProtoDelayConfiguration.minDelay = hostProtoDelayMinDelay
        configuration.hostProtoDelayConfiguration.maxDelay = hostProtoDelayMaxDelay
        configuration.hostProtoDelayConfiguration.threshold = hostProtoDelayThreshold
        configuration.hostProtoDelayConfiguration.base = hostProtoDelayBase
        configuration.hostCommDelayType = hostCommDelayType  # the default communication delay is 1
        configuration.hostCommDelayConfiguration = HostCommDelayConfiguration()  # not used so far
        configuration.layoutType = LayoutType.UNIFORM  # generates the hosts uniformly over the area

        configuration.linkType = LinkType.DEFAULT
        configuration.topologyType = TopologyType.BETA_SKELETON
        configuration.topologyConfiguration.beta = beta  # network density: 0.7-2

        configuration.routingType = RoutingType.AODV
        configuration.routingConfiguration.sinkId = 0
        configuration.routingConfiguration.communicationModel = CommunicationModel.SINK
        configuration.routingConfiguration.destinationOnlyFlag = destinationOnlyFlag

        configuration.metricType = MetricType.ALL_METRIC

        configuration.scenarioType = scenarioType
        configuration.scenarioConfiguration.ratio = scenarioRatio
        configuration.scenarioConfiguration.pdrMin = scenarioPdrMin
        configuration.scenarioConfiguration.pdrMax = scenarioPdrMax
        configuration.results = Results()

        return configuration



import json

def toJson(obj):
    return json.dumps(obj, default=lambda obj: obj.__dic__)

class MultiFactory:

    def __init__(self):
        self._hostFactory: HostFactory = None
        self._linkFactory: LinkFactory = None
        self._layoutFactory: LayoutFactory = None
        self._topologyFactory: TopologyFactory = None
        self._routingFactory: RoutingFactory = None
        self._metricFactory: MetricFactory = None
        self._scenarioFactory: ScenarioFactory = None

    def createScenario(self, configuration=Configuration()):
        self._hostFactory = HostFactory(configuration.seedId,
                                        configuration.hostRoutingType,
                                        configuration.hostProtoDelayType,
                                        configuration.hostProtoDelayConfiguration,
                                        configuration.hostCommDelayType,
                                        configuration.hostCommDelayConfiguration)
        self._layoutFactory = LayoutFactory(configuration.seedId,
                                            configuration.hostQuantity,
                                            configuration.xSize,
                                            configuration.ySize,
                                            configuration.layoutType)
        self._linkFactory = LinkFactory(configuration.linkType)
        self._topologyFactory = TopologyFactory(configuration.topologyType)
        self._routingFactory = RoutingFactory(configuration.routingType,
                                              configuration.routingConfiguration)
        self._metricFactory = MetricFactory(configuration.metricType)
        self._scenarioFactory = ScenarioFactory(configuration.scenarioType)

        layout = self._layoutFactory.create(self._hostFactory)
        topology = self._topologyFactory.create(layout, self._linkFactory, configuration.topologyConfiguration)
        routing = self._routingFactory.create(topology)
        metric = self._metricFactory.create(routing)
        scenario = self._scenarioFactory.create(metric,
                                                configuration.seedId,
                                                configuration.results,
                                                configuration.scenarioConfiguration)
        # if configuration.plot:
        #     scenario.plot()
        #     plt.savefig("{0}/{1}.png".format(configuration.resultsDirectory, configuration.seedId))
        return scenario


class Simulation:
    def __init__(self, configuration):
        self._name = configuration.simulationName
        self._configuration = configuration
        self._results = configuration.results
        self._outputDirectory = None
        self._prepareOutputDirectory(configuration.simulationName,
                                     configuration.resultsDirectory)
        configuration.saveToFile()

        # Disables QT warning messages
        environ["QT_DEVICE_PIXEL_RATIO"] = "0"
        environ["QT_AUTO_SCREEN_SCALE_FACTOR"] = "1"
        environ["QT_SCREEN_SCALE_FACTORS"] = "1"
        environ["QT_SCALE_FACTOR"] = "1"
        environ["QT_LOGGING_RULES"] = "qt5ct.debug=false"

    def run(self,
            silent=False):
        # Logger.resultsDirectory = configuration.resultsDirectory
        # Logger.simulationName = self._name
        # Logger.start()
        # configuration.results = Results()

        scenarios = []
        multiFactory = MultiFactory()
        startId = self._configuration.startId
        quantity = self._configuration.quantity
        for id in range(startId, startId + quantity):
            self._configuration.seedId = id
            scenarios.append(multiFactory.createScenario(self._configuration))

        # Logger.log("Simulation - {0}".format(self._name))
        # Logger.log("\tstartId: {0}".format(startId))
        # Logger.log("\tquantity: {0}".format(quantity))

        # if not silent:
        #     scenarios[0].printHeader()
        print("{0}:".format(self._name))
        for item in scenarios[0].getHeader():
            print("{:<4}\t".format(item), end="")
        print()
        for scenario in scenarios:
            # Logger.id = scenario.getId()
            scenario.execute(silent)
            if self._configuration.plot:
                scenario.plot()
                plt.savefig("{0}/{1}.svg".format(self._outputDirectory,
                                                 scenario.getId()),
                            dpi=200)
            if not silent:
                print(self._results.getResult(scenario.getId()).getValues())
            # scenario.getMetric().getRouting().saveToFile(self._outputDirectory + "/{0}.routes".format(scenario.getId()))
        # scenarios[0].printFinalStats(configuration.results)
        self._results.saveToFile(self._outputDirectory)
        print(self._results.getStatsText())
        # Logger.stop()

    def _prepareOutputDirectory(self,
                                simulationName,
                                resultsDirectory):
        pathToDir = "{0}/{1}".format(resultsDirectory,
                                     simulationName)
        if not os.path.isdir(pathToDir):
            os.makedirs(pathToDir)
        files = glob.glob(pathToDir + "/*")
        for file in files:
            os.remove(file)
        self._outputDirectory = pathToDir

class Logger:

    resultsDirectory = None
    simulationName = None
    id = None
    _fileOpen = False

    @classmethod
    def start(cls):
        # path must exists
        cls._file = open("{0}/{1}/log".format(cls.resultsDirectory, cls.simulationName), "w")
        cls._fileOpen = True

    @classmethod
    def log(cls, text):
        print(text)
        cls._file.write(text + "\n")

    @classmethod
    def stop(cls):
        cls._file.close()
        cls._fileOpen = False

if __name__ == '__main__':
    print("test")

# See PyCharm help at https://www.jetbrains.com/help/pycharm/

