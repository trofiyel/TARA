#!/bin/bash

if [ $# -lt 2 ]; then
  echo "Usage: script <input data file> <output file> [X-desc] [Y-desc] [Z-desc] [x-range] [y-range] [z-range]"
  exit 1
fi
echo "$6"
gnuplot -e "
  set xlabel \"$3\";
  set ylabel \"$4\";
  set zlabel \"$5\" rotate by 90;
  set xrange $6;
  set yrange $7;
  set zrange $8;
  set term svg;
  set output \"$2\";
  splot \"$1\" with lines notitle;
  pause -1"