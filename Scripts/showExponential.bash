#!/bin/bash

RESULTS_DIR='../R-Exponential-SC0.4-DFT'

if [ $# -gt 0 ]; then
  RESULTS_DIR="$1"
fi



cd "${RESULTS_DIR}"

PDRavg="PDRavg"
PLavg="PLavg"
RDDavg="RDDavg"
RDDmax="RDDmax"
BASE="BASE"
MD="MD"
printf "%s %s %s %s %s %s\n" "$BASE" "$MD" "$PDRavg" "$PLavg" "$RDDavg" "$RDDmax"
#printf "%s\n" "$RDDmax"

for DIR in *; do

  if ! [ -d "$DIR" ]; then
    continue
  fi

  PDRavg=$(grep '^AVG:'  "${DIR}/results" | cut -f4)
  PLavg=$(grep '^AVG:'  "${DIR}/results" | cut -f10)
  RDDavg=$(grep '^AVG:'  "${DIR}/results" | cut -f16)
  RDDmax=$(grep '^MAX:'  "${DIR}/results" | cut -f21)
  if [ "$DIR" != "Control" ]; then
    BASE=$(cut -c6-9 <<< "$DIR")
    MD=$(cut -c14- <<< "$DIR")
  else
    BASE="-"
    MD="-"
  fi

printf "%s %s %s %s %s %s\n" "$BASE" "$MD" "$PDRavg" "$PLavg" "$RDDavg" "$RDDmax"
#  printf "%s\n" "$RDDmax"
done