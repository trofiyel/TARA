#!/bin/bash

if [ $# -lt 1 ]; then
  echo "Usage: script <data files directory>"
  exit 1
fi

OUTPUT_SECTION_DELIMITER="512"

cd "$1"

for FILE in *; do
  if [ -d "$FILE" ]; then
    continue
  fi
  if ! grep -q '\.data$' <<< "$FILE"; then
    continue
  fi
  awk '{if($2 == "'"$OUTPUT_SECTION_DELIMITER"'"){
          print;
          printf "\n"
        } else {
          print
        }
       }' "$FILE" > "${FILE}.prepared"
done