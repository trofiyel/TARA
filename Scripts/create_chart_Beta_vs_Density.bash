#!/bin/bash
# To generate data for this script run test "testDensityDistribution()"

TMP_FILE=".tmp"
>"$TMP_FILE"

for DIR in ../Results/Density*; do
  BETA=$(cut -d" " -f5 <<< "$DIR")
  DEN=$(grep "^AVG" "${DIR}/results" | cut -f2)
  echo $BETA $DEN >> "$TMP_FILE"
done

gnuplot -e "
  set xlabel '{/Symbol b} [-]';
  set ylabel 'Density [-]';
  set title 'Relation between beta and network density';
  plot '$TMP_FILE' with linespoints lc rgb \"black\" notitle;
  pause -1"