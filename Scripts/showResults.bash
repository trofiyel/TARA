#!/bin/bash

#RESULTS_DIR='Testing1'
#RESULTS_DIR='R-Linear'
#RESULTS_DIR='R-Linear-10_hosts'
#RESULTS_DIR='R-Constant-delay'
#RESULTS_DIR='R-Linear'
RESULTS_DIR='R-Exponential'
#FILTER="M.-S2-DFT-[1]\.[0123456789]"
#FILTER="THR:.\...-CD:40"
#FILTER="THR:.\...-MD:10"
FILTER="BASE:...-MD:320"
#FILTER=""

cd "../${RESULTS_DIR}/"

for DIR in *; do
  if grep -q "$FILTER" <<< "$DIR"; then
    PDRavg=$(grep '^AVG:'  "../${RESULTS_DIR}/${DIR}/results" | cut -f4)
    PLavg=$(grep '^AVG:'  "../${RESULTS_DIR}/${DIR}/results" | cut -f10)
    PDavg=$(grep '^AVG:'  "../${RESULTS_DIR}/${DIR}/results" | cut -f16)
    printf "%s: %s %s %s\n" "$DIR" "$PDRavg" "$PLavg" "$PDavg"
  fi
done