#!/bin/bash

if [ $# -lt 1 ]; then
  echo "Usage: script <path to the input directory with results>"
  exit 1
fi

INPUT="$1"
OUTPUT_DIR="$1"
OUTPUT_SECTION_DELIMITER="512"

bash showExponential.bash "$INPUT" | \
  grep "^[0123456789]" | \
  cut -d' ' -f1,2,3 | \
  awk '{if($2 == "'"$OUTPUT_SECTION_DELIMITER"'"){
          print;
          printf "\n"
        } else {
          print
        }
       }' > "${OUTPUT_DIR}/graph-PDRavg.data"

gnuplot -e "
  set title '$INPUT';
  set term png;
  set output '${OUTPUT_DIR}/graph-PDRavg.data.png';
  splot '${OUTPUT_DIR}/graph-PDRavg.data' with lines;
  "

bash showExponential.bash "$INPUT" | \
  grep "^[0123456789]" | \
  cut -d' ' -f1,2,5 | \
  awk '{if($2 == "'"$OUTPUT_SECTION_DELIMITER"'"){
          print;
          printf "\n"
        } else {
          print
        }
       }' > "${OUTPUT_DIR}/graph-RDDavg.data"

gnuplot -e "
  set title '$INPUT';
  set term png;
  set output '${OUTPUT_DIR}/graph-RDDavg.data.png';
  splot '${OUTPUT_DIR}/graph-RDDavg.data' with lines;
  "
