#!/bin/bash

if [ $# -lt 1 ]; then
  echo "Usage: script <name of data file>"
  exit 1
fi

gnuplot -e "
  splot \"$1\" with lines;
  pause -1"