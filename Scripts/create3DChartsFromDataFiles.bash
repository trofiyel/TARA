#!/bin/bash

if [ $# -lt 1 ]; then
  echo "Usage: script <FILE files directory>"
  exit 1
fi

cd "$1"

OUTPUT_SECTION_DELIMITER="512"

for FILE in *; do
  if [ -d "$FILE" ]; then
    continue
  fi
  if ! grep -q '\.data$' <<< "$FILE"; then
    continue
  fi
  tail +2 "$FILE" |
  awk '{if($2 == "'"$OUTPUT_SECTION_DELIMITER"'"){
          print;
          printf "\n"
        } else {
          print
        }
       }' > "${FILE}.prepared"
  XLABEL=$(head -n 1 "$FILE" | cut -f1)
  YLABEL=$(head -n 1 "$FILE" | cut -f2)
  ZLABEL=$(head -n 1 "$FILE" | cut -f3)

  FILE_NAME=$(cut -d. -f1 <<< "$FILE")
  gnuplot -e "
  set title '$FILE_NAME';
  set term svg;
  set xlabel \"$XLABEL\" offset 0,-1;
  set ylabel \"$YLABEL\" offset 3,-1;
  set zlabel \"$ZLABEL\" rotate by 90;
  set output '${FILE_NAME}.svg';
  splot '${FILE}.prepared' with lines notitle;
  "
done