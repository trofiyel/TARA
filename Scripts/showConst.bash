#!/bin/bash

RESULTS_DIR='R-Constant-delay-DFF'

cd "../${RESULTS_DIR}/"

PDRavg="PDRavg"
PLavg="PLavg"
RDDavg="RDDavg"
RDDmax="RDDmax"
THR="THR"
CD="CD"
printf "%s %s %s %s %s %s\n" "$THR" "$CD" "$PDRavg" "$PLavg" "$RDDavg" "$RDDmax"

for DIR in *; do
  PDRavg=$(grep '^AVG:'  "../${RESULTS_DIR}/${DIR}/results" | cut -f4)
  PLavg=$(grep '^AVG:'  "../${RESULTS_DIR}/${DIR}/results" | cut -f10)
  RDDavg=$(grep '^AVG:'  "../${RESULTS_DIR}/${DIR}/results" | cut -f16)
  RDDmax=$(grep '^MAX:'  "../${RESULTS_DIR}/${DIR}/results" | cut -f21)

  if [ "$DIR" != "Control" ]; then
    THR=$(cut -c5-8 <<< "$DIR")
    CD=$(cut -c13- <<< "$DIR")
  else
    THR="-"
    CD="-"
  fi

  printf "%s %s %s %s %s %s\n" "$THR" "$CD" "$PDRavg" "$PLavg" "$RDDavg" "$RDDmax"
done