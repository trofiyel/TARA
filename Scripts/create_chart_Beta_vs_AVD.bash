#!/bin/bash
# To generate data for this script run test "testDensityDistribution()"

TMP_FILE=".tmp"
>"$TMP_FILE"

for DIR in ../R-Old/Results/Density*; do
  BETA=$(cut -d" " -f5 <<< "$DIR")
  AVD=$(grep "^AVG" "${DIR}/results" | cut -f3)
  echo $BETA $AVD >> "$TMP_FILE"
done

gnuplot -e "
  set xlabel '{/Symbol b} [-]';
  set ylabel 'Density [-]';
  set title 'Relation between beta and average vertex degree';
  plot '$TMP_FILE' with linespoints lc rgb \"black\" notitle;
  pause -1"