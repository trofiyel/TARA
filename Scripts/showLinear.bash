#!/bin/bash

RESULTS_DIR='R-Linear-quantity-dependency-DFT'

cd "../${RESULTS_DIR}/"

PDRavg="PDRavg"
PLavg="PLavg"
RDDavg="RDDavg"
RDDmax="RDDmax"
THR="THR"
MD="MD"
printf "%s %s %s %s %s %s\n" "$THR" "$MD" "$PDRavg" "$PLavg" "$RDDavg" "$RDDmax"

for DIR in *; do
  PDRavg=$(grep '^AVG:'  "../${RESULTS_DIR}/${DIR}/results" | cut -f4)
  PLavg=$(grep '^AVG:'  "../${RESULTS_DIR}/${DIR}/results" | cut -f10)
  RDDavg=$(grep '^AVG:'  "../${RESULTS_DIR}/${DIR}/results" | cut -f16)
  RDDmax=$(grep '^MAX:'  "../${RESULTS_DIR}/${DIR}/results" | cut -f21)

  if [ "$DIR" != "Control" ]; then
    THR=$(cut -c5-8 <<< "$DIR")
    MD=$(cut -c13- <<< "$DIR")
  else
    THR="-"
    MD="-"
  fi

  printf "%s %s %s %s %s %s\n" "$THR" "$MD" "$PDRavg" "$PLavg" "$RDDavg" "$RDDmax"
done